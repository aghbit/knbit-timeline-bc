package testdoubles.kafka

import kafka.utils.Time

/**
 * @author Lukasz Raduj 2015 raduj.lukasz at gmail.com
 */

class SystemTime implements Time {
    public long milliseconds() {
        return System.currentTimeMillis()
    }

    public long nanoseconds() {
        return System.nanoTime()
    }

    public void sleep(long ms) {
        try {
            Thread.sleep(ms)
        } catch (InterruptedException e) {
            // Ignore
        }
    }
}

