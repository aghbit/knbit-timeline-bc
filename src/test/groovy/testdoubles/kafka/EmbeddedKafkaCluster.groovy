package testdoubles.kafka

import groovy.util.logging.Slf4j
import kafka.admin.AdminUtils
import kafka.server.KafkaConfig
import kafka.server.KafkaServer
import kafka.utils.ZKStringSerializer$
import org.I0Itec.zkclient.ZkClient

/**
 * @author Lukasz Raduj 2015 raduj.lukasz at gmail.com
 */
@Slf4j
class EmbeddedKafkaCluster {
    List<Integer> ports
    String zkConnection
    Properties baseProperties
    String brokerList

    List<KafkaServer> brokers
    List<File> logDirs


    EmbeddedKafkaCluster(String zkConnection, Properties baseProperties, List<Integer> ports) {
        this.zkConnection = zkConnection
        this.ports = resolvePorts(ports)
        this.baseProperties = baseProperties

        this.brokers = []
        this.logDirs = []

        this.brokerList = constructBrokerList(this.ports)
    }

    def void createTopics(List<String> topics) {
        log.info("Creating topics {}", topics)
        int sessionTimeoutMs = 10000
        int connectionTimeoutMs = 10000
        ZkClient zkClient = new ZkClient(zkConnection, sessionTimeoutMs, connectionTimeoutMs, ZKStringSerializer$.MODULE$)

        int numPartitions = 1
        int replicationFactor = 1
        Properties topicConfig = new Properties()

        topics.each { topic -> AdminUtils.createTopic(zkClient, topic, numPartitions, replicationFactor, topicConfig) }
    }

    static def List<Integer> resolvePorts(List<Integer> ports) {
        List<Integer> resolvedPorts = new ArrayList<>()
        for (Integer port : ports) {
            resolvedPorts.add(resolvePort(port))
        }
        return resolvedPorts
    }

    static def int resolvePort(int port) {
        if (port == -1) {
            return TestUtils.getAvailablePort()
        }
        return port
    }

    static def String constructBrokerList(List<Integer> ports) {
        StringBuilder sb = new StringBuilder()
        for (Integer port : ports) {
            if (sb.length() > 0) {
                sb.append(",")
            }
            sb.append("localhost:").append(port)
        }
        return sb.toString()
    }

    def void startup() {
        for (int i = 0; i < ports.size(); i++) {
            Integer port = ports.get(i)
            File logDir = TestUtils.constructTempDir("kafka-local")

            Properties properties = new Properties()
            properties.putAll(baseProperties)
            properties.setProperty("zookeeper.connect", zkConnection)
            properties.setProperty("broker.id", String.valueOf(i + 1))
            properties.setProperty("host.name", "localhost")
            properties.setProperty("port", Integer.toString(port))
            properties.setProperty("log.dir", logDir.getAbsolutePath())
            properties.setProperty("log.flush.interval.messages", String.valueOf(1))

            KafkaServer broker = startBroker(properties)

            brokers.add(broker)
            logDirs.add(logDir)
        }
    }


    static def KafkaServer startBroker(Properties props) {
        KafkaServer server = new KafkaServer(new KafkaConfig(props), new SystemTime())
        server.startup()
        return server
    }

    def Properties getProps() {
        Properties props = new Properties()
        props.putAll(baseProperties)
        props.put("metadata.broker.list", brokerList)
        props.put("zookeeper.connect", zkConnection)
        return props
    }

    def String getBrokerList() {
        return brokerList
    }

    def List<Integer> getPorts() {
        return ports
    }

    def String getZkConnection() {
        return zkConnection
    }

    def void shutdown() {
        for (KafkaServer broker : brokers) {
            try {
                broker.shutdown()
            } catch (Exception e) {
                e.printStackTrace()
            }
        }
        for (File logDir : logDirs) {
            try {
                TestUtils.deleteFile(logDir)
            } catch (FileNotFoundException e) {
                e.printStackTrace()
            }
        }
    }
}