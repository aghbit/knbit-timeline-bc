package pl.agh.knbit.domain.semester.systemevents.listeners

import pl.agh.knbit.domain.feeds.FeedRepository
import pl.agh.knbit.domain.semester.event.EventId
import pl.agh.knbit.domain.semester.event.EventType
import pl.agh.knbit.domain.semester.repository.SemesterRepository
import pl.agh.knbit.domain.semester.section.SectionId
import pl.agh.knbit.domain.semester.systemevents.SemesterFactory
import spock.lang.Specification
import spock.lang.Subject
import util.databuilder.EventsFixture
import util.databuilder.SemesterFixture

class EventTookPlaceListenerSpec extends Specification {

    @Subject
    EventTookPlaceListener underTest = new EventTookPlaceListener(
            Stub(SemesterRepository), Stub(SemesterFactory), Stub(FeedRepository)
    )

    def semester = SemesterFixture.fixedIdsSemester()

    def "Should add new event to matching semester"() {
        given:
        def event = EventsFixture.newEventTookPlaceEvent()

        def eventId = EventId.of(event.eventId)

        when:
        underTest.updateSemester(semester, event)

        then: 'event was added to semester'
        def addedEvent = semester.events().find { e -> e.eventId().equals(eventId) }

        and: 'event has correct fields set'
        addedEvent.title() == event.eventName
        addedEvent.description() == event.eventDescription
        addedEvent.eventType() == EventType.valueOf(event.eventType.name())
        addedEvent.pictureUrl() == event.eventPictureUrl
        addedEvent.sectionId() == SectionId.of(event.sectionId)
        addedEvent.speakers().size() == event.speakersCount
    }

    def "should build new feed from event"() {
        given:
        def event = EventsFixture.newEventTookPlaceEvent()

        when:
        def feed = underTest.buildFeed(event)

        then:
        feed.get().message == '50 people learnt cool things on Twitter Zipkin Intro! event'
    }
}
