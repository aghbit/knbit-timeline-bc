package pl.agh.knbit.domain.semester.systemevents.listeners

import pl.agh.knbit.domain.feeds.FeedRepository
import pl.agh.knbit.domain.semester.Member
import pl.agh.knbit.domain.semester.repository.SemesterRepository
import pl.agh.knbit.domain.semester.systemevents.SemesterFactory
import spock.lang.Specification
import spock.lang.Subject
import util.databuilder.EventsFixture
import util.databuilder.SemesterFixture

import java.time.LocalDateTime

import static java.time.Month.OCTOBER

class MemberLeftKNBITListenerSpec extends Specification {

    @Subject
    MemberLeftKNBITListener underTest = new MemberLeftKNBITListener(
            Stub(SemesterRepository), Stub(SemesterFactory), Stub(FeedRepository)
    )

    def semester = SemesterFixture.newSemester()

    def "Should remove member from proper semester"() {
        given:
        def initialMembersCount = semester.members().size()
        Member memberId = semester.members().first()

        LocalDateTime eventDateTime = LocalDateTime.of(2014, OCTOBER, 21, 0, 0)
        def memberLeftKnBitEvent = EventsFixture.newMemberLeftKNBITEvent(eventDateTime, memberId)

        when:
        underTest.updateSemester(semester, memberLeftKnBitEvent)

        then:
        semester.members().size() == initialMembersCount - 1
    }

    def "should build new feed from event"() {
        given:
        def event = EventsFixture.newMemberLeftKNBITEvent()

        when:
        def feed = underTest.buildFeed(event)

        then:
        feed.get().message == 'Member Janusz Bober left KN BIT. Reason NOT-PROVIDED'
    }
}
