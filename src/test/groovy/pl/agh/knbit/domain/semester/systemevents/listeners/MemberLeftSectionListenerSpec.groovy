package pl.agh.knbit.domain.semester.systemevents.listeners

import pl.agh.knbit.domain.feeds.FeedRepository
import pl.agh.knbit.domain.semester.repository.SemesterRepository
import pl.agh.knbit.domain.semester.systemevents.SemesterFactory
import spock.lang.Specification
import spock.lang.Subject
import util.databuilder.EventsFixture
import util.databuilder.SectionFixture
import util.databuilder.SemesterFixture

class MemberLeftSectionListenerSpec extends Specification {

    @Subject
    MemberLeftSectionListener underTest = new MemberLeftSectionListener(
            Stub(SemesterRepository), Stub(SemesterFactory), Stub(FeedRepository)
    )

    def semester = SemesterFixture.newSemester()


    def "Should increase members count in matching section"() {
        given: 'semester with one section and one member'
        def sectionId = SectionFixture.nextSectionId()
        semester.addSection(SectionFixture.newSection(sectionId))
        semester.findSectionById(sectionId).addMember()
        and: 'event matching that section'
        def event = EventsFixture.newMemberLeftSectionEvent(sectionId)

        when:
        underTest.updateSemester(semester, event)

        then: 'members count in section was increased'
        def section = semester.findSectionById(sectionId)
        section.memberCount() == 0
    }

    def "should build new feed from event"() {
        given:
        def section = semester.sections().first()
        def sectionId = section.sectionId()
        def memberId = EventsFixture.uuid()
        def event = EventsFixture.newMemberLeftSectionEvent(sectionId, memberId)
        underTest.sectionName = "BIT AI"

        when:
        def feed = underTest.buildFeed(event)

        then:
        feed.get().message == "Member of id: ${memberId} left section: BIT AI. Reason: No time left :/".toString()
    }
}
