package pl.agh.knbit.domain.semester.systemevents

import pl.agh.knbit.domain.semester.SemesterId
import pl.agh.knbit.domain.semester.repository.SemesterRepository
import spock.lang.Specification
import spock.lang.Subject
import util.databuilder.SemesterFixture

/**
 * @author Lukasz Raduj 2015 raduj.lukasz at gmail.com
 */
class SemesterFactoryTest extends Specification {

    @Subject
    SemesterFactory underTest

    def periodRepositoryMock = Mock(SemesterRepository)

    void setup() {
        underTest = new SemesterFactory(periodRepositoryMock)
    }

    def "Should return previous semester copy when previous exists"() {
        given:
        def newPeriodId = SemesterId.of("2014-SUMMER")

        and: 'existing previous semester'
        def previousPeriodId = SemesterId.of('2014-WINTER')
        def previousSemester = SemesterFixture.newSemester(previousPeriodId)
        periodRepositoryMock.exists(previousPeriodId) >> true
        periodRepositoryMock.findOne(previousPeriodId) >> previousSemester

        when:
        def semester = underTest.newDraftFromPreviousSemester(newPeriodId)

        then: 'id is as requested'
        semester.semesterId() == newPeriodId

        and: 'data from previous semester was copied'
        previousSemester.members() == semester.members()
        previousSemester.sections() == semester.sections()
        previousSemester.projects() == semester.projects()
    }

    def "Should create new, empty Semester when previous Semester does not exist"() {
        given:
        def newPeriodId = SemesterId.of('2014-SUMMER')

        when:
        def semester = underTest.newDraftFromPreviousSemester(newPeriodId)

        then:
        semester.semesterId() == newPeriodId

        and:
        semester.members().isEmpty()
        semester.events().isEmpty()
        semester.projects().isEmpty()
        semester.sections().isEmpty()
    }
}
