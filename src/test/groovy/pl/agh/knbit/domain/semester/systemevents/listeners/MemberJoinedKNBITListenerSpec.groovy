package pl.agh.knbit.domain.semester.systemevents.listeners

import pl.agh.knbit.domain.feeds.FeedRepository
import pl.agh.knbit.domain.semester.repository.SemesterRepository
import pl.agh.knbit.domain.semester.systemevents.SemesterFactory
import spock.lang.Specification
import util.databuilder.EventsFixture
import util.databuilder.SemesterFixture

class MemberJoinedKNBITListenerSpec extends Specification {

    MemberJoinedKNBITListener underTest = new MemberJoinedKNBITListener(
            Stub(SemesterRepository), Stub(SemesterFactory), Stub(FeedRepository)
    )

    def semester = SemesterFixture.fixedIdsSemester()

    def "Should add new member to proper semester"() {
        given:
        def memberJoinedKnBitEvent = EventsFixture.newMemberJoinedKNBITEvent()

        and:
        def initialMembersCount = this.semester.members().size()

        when:
        def afterUpdateSemester = underTest.updateSemester(semester, memberJoinedKnBitEvent)

        then:
        afterUpdateSemester.members().size() == initialMembersCount + 1
    }

    def "should build new feed from event"() {
        given:
        def event = EventsFixture.newMemberJoinedKNBITEvent()

        when:
        def feed = underTest.buildFeed(event)

        then:
        feed.get().message == 'Mark Knopfler joined KN BIT. Welcome!'
    }
}
