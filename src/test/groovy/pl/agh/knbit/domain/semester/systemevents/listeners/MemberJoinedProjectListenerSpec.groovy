package pl.agh.knbit.domain.semester.systemevents.listeners

import pl.agh.knbit.domain.feeds.FeedRepository
import pl.agh.knbit.domain.semester.repository.SemesterRepository
import pl.agh.knbit.domain.semester.systemevents.SemesterFactory
import spock.lang.Specification
import util.databuilder.EventsFixture
import util.databuilder.SemesterFixture

class MemberJoinedProjectListenerSpec extends Specification {

    MemberJoinedProjectListener underTest = new MemberJoinedProjectListener(
            Stub(SemesterRepository), Stub(SemesterFactory), Stub(FeedRepository)
    )

    def semester = SemesterFixture.fixedIdsSemester()
    def project = semester.projects().first()
    def projectId = project.projectId()
    def memberId = EventsFixture.uuid()

    def "Should add new participant to proper project in semester"() {
        given: 'semester with some project'

        def memberJoinedProjectEvent = EventsFixture.newMemberJoinedProjectEvent(projectId, memberId)

        when: 'member joined that project event occurred'
        def afterUpdateSemester = underTest.updateSemester(semester, memberJoinedProjectEvent)

        then: 'semester should still have that project with participant from event'
        afterUpdateSemester.projects().find { p -> p.projectId().equals(projectId) }
                .participants().find { p -> p.memberId().equals(memberId) }
    }

    def "should build new feed from event"() {
        given:
        def event = EventsFixture.newMemberJoinedProjectEvent(projectId, memberId)

        when:
        def feed = underTest.buildFeed(event)

        then:
        feed.get().message == 'Mark Knopfler joined Decompiler project!'
    }
}
