package pl.agh.knbit.domain.semester.systemevents.listeners

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.annotation.DirtiesContext
import pl.agh.knbit.SpringTestBase
import pl.agh.knbit.domain.semester.SemesterId
import pl.agh.knbit.domain.semester.project.ProjectId
import pl.agh.knbit.domain.semester.repository.SemesterRepository
import pl.agh.knbit.domain.semester.repository.TransactionalSemesterRepository
import pl.agh.knbit.domain.semester.section.SectionId
import pl.agh.knbit.portadapter.kafka.KafkaEventProducer
import spock.lang.Stepwise
import util.databuilder.EventsFixture
import util.databuilder.SectionFixture
import util.databuilder.SemesterFixture

import static util.databuilder.EventsFixture.uuid

@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@Stepwise
class TopicsStartupBeanSpec extends SpringTestBase {

    @Autowired
    KafkaEventProducer producer

    @Autowired
    SemesterRepository semesterRepository

    @Autowired
    TransactionalSemesterRepository transactionalSemesterRepository

    def currentSemester = SemesterFixture.currentSemester()
    def currentSemesterId = currentSemester.semesterId()


    def "I'm here to just mark context as dirty"() {
        expect:
        true
    }

    def "Should build Semester Summary using system events"() {
        given: 'bunch of system events'
        def someSectionId = SectionFixture.nextSectionId()
        def getInTimeProjectId = ProjectId.of(uuid())
        def decompilerProjectId = ProjectId.of(uuid())
        def memberId = uuid()

        def events = [
                EventsFixture.newMemberJoinedKNBITEvent(),
                EventsFixture.newMemberJoinedKNBITEvent(),
                EventsFixture.newEventTookPlaceEvent(),
                EventsFixture.newEventTookPlaceEvent(),
                EventsFixture.newSectionCreatedEvent(),
                EventsFixture.newSectionCreatedEvent(someSectionId),
                EventsFixture.newMemberJoinedSectionEvent(someSectionId),
                EventsFixture.newProjectCreatedEvent(getInTimeProjectId),
                EventsFixture.newProjectCreatedEvent(decompilerProjectId),
                EventsFixture.newMemberJoinedProjectEvent(getInTimeProjectId, memberId)
        ]

        and: 'empty semesters repository'
        semesterRepository.deleteAllInBatch()

        when: 'events published to kafka'
        events.each {
            event -> producer.publish(event)
            sleep(10)
        }

        then: 'timeline-bc should handle them and build semester model'
        def retry = 5;
        while (retry--) {
            sleep(1500) //todo: change to exponential
            if (validate(someSectionId, getInTimeProjectId, memberId)) {
                assert true
                return
            }
        }

        assert false
    }

    def "Should remove member from section after MemberLeftSectionEvent occurred"() {
        given: 'semester with one section and one member in it'
        def sectionId = SectionFixture.nextSectionId()

        currentSemester.sections().clear()
        def section = SectionFixture.newSection(sectionId)
        section.addMember()
        currentSemester.addSection(section)
        semesterRepository.save(currentSemester)

        def event = EventsFixture.newMemberLeftSectionEvent(sectionId)

        when:
        producer.publish(event)

        then:
        sleep(500)

        def afterUpdateSemester = semesterRepository.findOne(currentSemesterId)
        afterUpdateSemester.sections().size() == 1
        afterUpdateSemester.findSectionById(sectionId).memberCount() == 0
    }

    private boolean validate(SectionId someSectionId, ProjectId getInTimeProjectId, String memberId) {
        try {
            def semester = transactionalSemesterRepository.findOneFullyLoaded(SemesterId.of('2015-WINTER'))

            def sectionWithMember = semester.findSectionById(someSectionId)

            def projectWithMember = semester.findProjectById(getInTimeProjectId)

            return semester != null &&
                    semester.members().size() == 2 &&
                    semester.events().size() == 2 &&
                    semester.sections().size() == 2 &&
                    sectionWithMember.memberCount() == 1 &&
                    semester.projects().size() == 2 &&
                    projectWithMember.participants().find {m -> m.memberId().equals(memberId)}
        } catch (Exception ignored) {
            return false
        }
    }
}
