package pl.agh.knbit.domain.semester.systemevents.listeners

import pl.agh.knbit.domain.feeds.FeedRepository
import pl.agh.knbit.domain.semester.repository.SemesterRepository
import pl.agh.knbit.domain.semester.section.SectionId
import pl.agh.knbit.domain.semester.systemevents.SemesterFactory
import spock.lang.Specification
import spock.lang.Subject
import util.databuilder.EventsFixture
import util.databuilder.SectionFixture
import util.databuilder.SemesterFixture

class SectionCreatedListenerSpec extends Specification {

    @Subject
    SectionCreatedListener underTest = new SectionCreatedListener(
            Stub(SemesterRepository), Stub(SemesterFactory), Stub(FeedRepository)
    )

    def semester = SemesterFixture.fixedIdsSemester()

    def "Should add new section to semester"() {
        given:
        def sectionId = SectionFixture.nextSectionId()
        def event = EventsFixture.newSectionCreatedEvent(sectionId)

        and:
        def sectionsCount = semester.sections().size()

        when:
        underTest.updateSemester(semester, event)

        then: 'section was added to semester'
        semester.sections().size() == sectionsCount + 1

        and: 'has all field correctly set'
        def section = semester.sections().find { s -> s.sectionId().equals(sectionId) }
        section.sectionId() == SectionId.of(event.sectionId)
        section.name() == event.sectionName
        section.description() == event.sectionDescription
        section.memberCount() == 0
        section.logoUrl() == event.logoUrl
    }

    def "should build new feed from event"() {
        given:
        def event = EventsFixture.newSectionCreatedEvent()

        when:
        def feed = underTest.buildFeed(event)

        then:
        feed.get().message == "New section created! Now you can join brand new Idea Factory section"
    }
}
