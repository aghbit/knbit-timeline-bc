package pl.agh.knbit.domain.semester.systemevents.listeners

import pl.agh.knbit.domain.feeds.FeedRepository
import pl.agh.knbit.domain.semester.SemesterId
import pl.agh.knbit.domain.semester.repository.SemesterRepository
import pl.agh.knbit.domain.semester.systemevents.SemesterFactory
import pl.agh.knbit.generated.protobuffs.TimelineBc
import pl.agh.knbit.util.DateTimeUtils
import spock.lang.Specification
import spock.lang.Subject
import util.databuilder.SemesterFixture

class TotalCommitsCountListenerSpec extends Specification {

    SemesterRepository semesterRepositoryStub = Stub(SemesterRepository)

    @Subject
    TotalCommitsCountListener underTest = new TotalCommitsCountListener(
            semesterRepositoryStub, Stub(SemesterFactory), Stub(FeedRepository)
    )

    def PREVIOUS_SEMESTER_COMMITS_COUNT = 3000

    void setup() {
        def previousSemesterId = SemesterId.of('2014-summer')
        semesterRepositoryStub.exists(previousSemesterId) >> true

        def semester = SemesterFixture.newSemester()
        semester.increaseCommitsCount(PREVIOUS_SEMESTER_COMMITS_COUNT)
        semesterRepositoryStub.findOne(previousSemesterId) >> semester
    }

    def semester = SemesterFixture.fixedIdsSemester()

    def "Should update commits count subtracting commits count from previous semester"() {
        given:
        long COMMITS_COUNT = 123000

        def event = TimelineBc.TotalCommitsCountUpdateEvent.newBuilder()
                .setUtcDateAsEpochSeconds(DateTimeUtils.nowAsEpochSeconds())
                .setCommitsCount(COMMITS_COUNT)
                .build()

        when:
        underTest.updateSemester(semester, event)

        then: 'commits count was updated'
        semester.commitsCount() == COMMITS_COUNT - PREVIOUS_SEMESTER_COMMITS_COUNT
    }
}
