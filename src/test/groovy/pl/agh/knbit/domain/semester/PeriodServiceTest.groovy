package pl.agh.knbit.domain.semester

import pl.agh.knbit.domain.semester.repository.SemesterRepository
import spock.lang.Specification
import spock.lang.Subject
import util.databuilder.SemesterFixture

/**
 * @author Lukasz Raduj 2015 raduj.lukasz at gmail.com
 */
class PeriodServiceTest extends Specification {

    def periodRepository = Stub(SemesterRepository)

    @Subject
    PeriodService underTest = new PeriodService(periodRepository)

    def "Should return all semesters ids"() {
        given:
        def firstSemester = SemesterFixture.newSemester()
        def secondSemester = SemesterFixture.newSemester()
        def thirdSemester = SemesterFixture.currentSemester()

        periodRepository.exists(firstSemester.semesterId()) >> true
        periodRepository.exists(secondSemester.semesterId()) >> true
        periodRepository.exists(thirdSemester.semesterId()) >> true

        when:
        def ids = underTest.findAllExistingPeriodIds()

        then:
        ids.collect { it -> it.semesterId } as Set == [firstSemester.semesterId(),
                                                     secondSemester.semesterId(),
                                                     thirdSemester.semesterId()] as Set
    }
}
