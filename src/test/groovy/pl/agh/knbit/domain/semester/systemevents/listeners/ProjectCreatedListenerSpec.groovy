package pl.agh.knbit.domain.semester.systemevents.listeners

import pl.agh.knbit.domain.feeds.FeedRepository
import pl.agh.knbit.domain.semester.project.ProjectId
import pl.agh.knbit.domain.semester.repository.SemesterRepository
import pl.agh.knbit.domain.semester.systemevents.SemesterFactory
import spock.lang.Specification
import spock.lang.Subject
import util.databuilder.EventsFixture
import util.databuilder.SemesterFixture

class ProjectCreatedListenerSpec extends Specification {

    @Subject
    ProjectCreatedListener underTest = new ProjectCreatedListener(
            Stub(SemesterRepository), Stub(SemesterFactory), Stub(FeedRepository)
    )

    def "should add new project to existing semester"() {
        given:
        def semester = SemesterFixture.currentSemester()

        def projectId = ProjectId.of(UUID.randomUUID().toString())
        def event = EventsFixture.newProjectCreatedEvent(projectId)

        when:
        underTest.updateSemester(semester, event)

        then:
        semester.projects().find { p -> p.projectId().equals(projectId) }
    }

    def "should build new feed from event"() {
        given:
        def event = EventsFixture.newProjectCreatedEvent()

        when:
        def feed = underTest.buildFeed(event)

        then:
        feed.get().message == 'New project Get In Time created! Join now! Our client is Get in Bank!'
    }
}
