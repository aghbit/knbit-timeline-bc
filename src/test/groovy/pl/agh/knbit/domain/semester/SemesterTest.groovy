package pl.agh.knbit.domain.semester

import spock.lang.Specification
import util.databuilder.SemesterFixture

/**
 * @author Lukasz Raduj 2015 raduj.lukasz at gmail.com
 */
class SemesterTest extends Specification {

    def "Should set correct year and season based on PeriodId"() {
        given:
        def periodId = SemesterId.of('2014-SUMMER')

        when:
        def semester = new Semester(periodId)

        then:
        semester.year() == 2014
        semester.season() == Season.SUMMER
    }

    def "Should mark newly created Semester as draft"() {
        given:
        def periodId = SemesterId.of('2013-WINTER')

        when:
        def semester = new Semester(periodId)

        then:
        semester.isDraft()
    }

    def "Should became a non-draft after activation"() {
        given:
        def draftSemester = SemesterFixture.newDraftSemester()

        when:
        draftSemester.activate()

        then:
        !draftSemester.isDraft()
    }
}
