package pl.agh.knbit.domain.semester

import spock.lang.Specification
import spock.lang.Unroll

import java.time.LocalDate
import java.time.Month

/**
 * @author Lukasz Raduj 2015 raduj.lukasz at gmail.com
 */
@Unroll
class SemesterIdTest extends Specification {

    def "Should not throw exception when id of correct pattern given"() {
        when:
        SemesterId.of(givenId)

        then:
        notThrown(Exception)

        where:
        givenId << ['2000-WINTER', '2015-SUMMER', '2014-summer', '2013-winter']
    }

    def "Should throw exception on incorrect id format"() {
        when:
        SemesterId.of(givenId)

        then:
        thrown(IllegalArgumentException)

        where:
        givenId << ['11-SUMMER', '2011-SPRING', '2015-winder']
    }

    def "Should return next chronologically correct id"() {
        when:
        def nextPeriodId = givenId.next()

        then:
        nextPeriodId == exptectedNextId

        where:
        givenId                    || exptectedNextId
        SemesterId.of('2000-SUMMER') || SemesterId.of('2001-WINTER')
        SemesterId.of('2000-WINTER') || SemesterId.of('2000-SUMMER')
    }

    def "Should return previous chronologically correct id"() {
        when:
        def nextPeriodId = givenId.previous()

        then:
        nextPeriodId == exptectedNextId

        where:
        givenId                    || exptectedNextId
        SemesterId.of('2000-SUMMER') || SemesterId.of('2000-WINTER')
        SemesterId.of('2001-WINTER') || SemesterId.of('2000-SUMMER')
    }

    def "Should correctly parse PeriodId from #givenDate LocalDate"() {
        when:
        def periodId = SemesterId.of(givenDate)

        then:
        periodId == expectedPeriodId

        where:
        givenDate                             || expectedPeriodId
        LocalDate.of(2015, Month.OCTOBER, 20) || SemesterId.of('2015-WINTER')
        LocalDate.of(2016, Month.JANUARY, 20) || SemesterId.of('2015-WINTER')
        LocalDate.of(2012, Month.MAY, 20)     || SemesterId.of('2011-SUMMER')
    }
}
