package pl.agh.knbit.domain.semester.systemevents.listeners

import pl.agh.knbit.domain.feeds.FeedRepository
import pl.agh.knbit.domain.semester.repository.SemesterRepository
import pl.agh.knbit.domain.semester.systemevents.SemesterFactory
import spock.lang.Specification
import spock.lang.Subject
import util.databuilder.EventsFixture
import util.databuilder.SemesterFixture

class MemberJoinedSectionListenerSpec extends Specification {

    @Subject
    MemberJoinedSectionListener underTest = new MemberJoinedSectionListener(
            Stub(SemesterRepository), Stub(SemesterFactory), Stub(FeedRepository)
    )

    def semester = SemesterFixture.fixedIdsSemester()
    def section = semester.sections().first()
    def sectionId = section.sectionId()

    def event = EventsFixture.newMemberJoinedSectionEvent(sectionId)

    def "should add new member to section "() {
        given: 'semester with section'
        and: 'initial member count in that section'
        def initialMemberCount = section.memberCount()

        when:
        underTest.updateSemester(semester, event)

        then:
        def afterUpdateSemester = semester.sections().find { s -> s.sectionId().equals(sectionId) }

        and:
        afterUpdateSemester.memberCount() == initialMemberCount + 1
    }

    def "should build new feed from event"() {
        when:
        def feed = underTest.buildFeed(event)

        then:
        feed.get().message == "Member Eric Clapton joined Idea factory section!"
    }
}
