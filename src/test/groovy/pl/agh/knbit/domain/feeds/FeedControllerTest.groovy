package pl.agh.knbit.domain.feeds

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import pl.agh.knbit.SpringTestBase
import util.databuilder.FeedFixture

import java.time.LocalDateTime

import static com.jayway.restassured.RestAssured.given
import static util.custommatcher.TimelineJsonMatcher.equalJson

class FeedControllerTest extends SpringTestBase {

    @Autowired
    FeedRepository feedRepository

    @Override
    void setup() {
        feedRepository.deleteAllInBatch()
    }

    def "Should have endpoint returning last feeds"() {
        given:
        def request = given()

        when:
        def response = request.get("/api/v3/feeds")

        then:
        response.statusCode == HttpStatus.OK.value()
    }

    def "Should return page of last feeds"() {
        given:
        def dateTime = LocalDateTime.of(2015, 10, 19, 12, 12, 13)
        25.times {
            feedRepository.save(FeedFixture.newFeed(dateTime))
            dateTime = dateTime.plusSeconds(1)
        }

        def request = given()

        when:
        def response = request.get("/api/v3/feeds?page=0")

        then:
        response.statusCode == HttpStatus.OK.value()
        equalJson(response.getBody().asString()).matchesFile('json/feed/feed-1-page.json')
    }

    def "should return only public feeds when user has no permissions"() {
        given:
        def dateTime = LocalDateTime.of(2015, 10, 19, 12, 12, 13)

        feedRepository.save(FeedFixture.newMemberLeftKNBITFeed(dateTime))

        10.times {
            dateTime = dateTime.plusDays(1)
            feedRepository.save(FeedFixture.newFeed(dateTime))
        }
        def request = given()

        when:
        def response = request.get("/api/v3/feeds?page=0")

        then:
        response.statusCode == HttpStatus.OK.value()
        equalJson(response.getBody().asString()).matchesFile('json/feed/only-public-feeds.json')
    }

    def "Should return first (no. 0) page when no query param passed"() {
        given:
        def dateTime = LocalDateTime.of(2015, 10, 19, 12, 12, 13)
        25.times {
            feedRepository.save(FeedFixture.newFeed(dateTime))
            dateTime = dateTime.plusSeconds(1)
        }

        def request = given()

        when:
        def response = request.get("/api/v3/feeds")

        then:
        response.statusCode == HttpStatus.OK.value()
        equalJson(response.getBody().asString()).matchesFile('json/feed/feed-0-page.json')
    }
}
