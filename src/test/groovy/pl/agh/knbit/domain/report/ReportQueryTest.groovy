package pl.agh.knbit.domain.report

import pl.agh.knbit.util.DateTimeUtils
import spock.lang.Specification
import spock.lang.Unroll

import java.time.LocalDate
import java.time.LocalDateTime

@Unroll
class ReportQueryTest extends Specification {

    def "Should return #expectedAnswer when event happened at #eventDate"() {
        given:
        def reportStartDate = LocalDate.of(2014, 1, 1)
        def reportEndDate = LocalDate.of(2014, 12, 31)

        when:
        def query = new ReportQuery(reportStartDate, reportEndDate)

        then:
        expectedAnswer == query.wasEventBetweenQueryDates(DateTimeUtils.toEpochSeconds(eventDate))

        where:
        eventDate                            | expectedAnswer
        LocalDateTime.of(2014, 5, 20, 0, 0)  | true
        LocalDateTime.of(2013, 12, 31, 0, 0) | false
        LocalDateTime.of(2015, 1, 1, 0, 0)   | false
        LocalDateTime.of(2014, 1, 2, 0, 0)   | true
        LocalDateTime.of(2014, 12, 30, 0, 0) | true
    }
}
