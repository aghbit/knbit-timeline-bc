package pl.agh.knbit.domain.report

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import pl.agh.knbit.SpringTestBase
import pl.agh.knbit.domain.semester.event.EventId
import pl.agh.knbit.domain.semester.project.ProjectId
import pl.agh.knbit.domain.semester.section.SectionId
import pl.agh.knbit.portadapter.kafka.KafkaEventProducer
import util.databuilder.EventsFixture

import java.time.LocalDateTime

import static com.jayway.restassured.RestAssured.given
import static util.custommatcher.TimelineJsonMatcher.equalJson

class ReportsControllerTest extends SpringTestBase {

    @Autowired
    KafkaEventProducer producer

    def "Should generate report"() {
        given:
        def request = given()

        when:
        def response = request.get("/api/v3/reports?start-date=2014-01-01&end-date=2016-12-31")

        then:
        response.statusCode() == HttpStatus.OK.value()
    }

    def "Should generate report with members counted"() {
        given: 'bunch of events on kafka'

        def ideaFactorySectionId = SectionId.of('afe0796e-ba3b-469e-92ca-088d6c936a4b')
        def aiSectionId = SectionId.of('22ac3cb1-6214-4ccf-9176-b3634aeb3210')

        def getInTimeProjectId = ProjectId.of('7d7eaf8b-0e12-47b4-9f38-44394c96c460')
        def obierakiProjectId = ProjectId.of('b99dce61-9ffd-483a-9f5d-c2d7315a48ea')

        def kafkaWorkshopId = EventId.of('bc84c55d-2a79-4e23-90fa-a0c691ddddee')
        def javaLectureId = EventId.of('8a07ef17-2150-4dca-a6a8-9fadeca966c4')

        def events = [
                EventsFixture.newSectionCreatedEvent(ideaFactorySectionId),
                EventsFixture.newSectionCreatedEvent(aiSectionId),
                EventsFixture.newProjectCreatedEvent(getInTimeProjectId),
                EventsFixture.newProjectCreatedEvent(obierakiProjectId),
                EventsFixture.newEventTookPlaceEvent(kafkaWorkshopId),
                EventsFixture.newEventTookPlaceEvent(javaLectureId)
        ]

        def id = 1;
        25.times {
            events.add(EventsFixture.newMemberJoinedKNBITEvent(String.valueOf(id++)))
        }

        //matching sectionId
        10.times {
            events.add(EventsFixture.newMemberJoinedSectionEvent(ideaFactorySectionId))
        }

        //not matching sectionId
        10.times {
            events.add(EventsFixture.newMemberJoinedSectionEvent(SectionId.of('not-existing-section-id')))
        }

        3.times {
            events.add(EventsFixture.newMemberLeftSectionEvent(ideaFactorySectionId))
        }

        def memberId = 1;
        12.times {
            events.add(EventsFixture.newMemberJoinedProjectEvent(getInTimeProjectId, String.valueOf(memberId++)))
        }

        events.each { e -> producer.publish(e) }

        def request = given()
        sleep(1500)

        when:
        def response = request.get("/api/v3/reports?start-date=2014-01-01&end-date=2015-12-31")

        then:
        response.statusCode == HttpStatus.OK.value()
        equalJson(response.getBody().asString()).matchesFile('json/report.json')
    }

    def "Should ignore events happened not between query dates"() {
        given:

        def eventId = EventId.of('bd57c01f-170a-4bca-92e9-0663cc54838d')
        def events = [
                EventsFixture.newEventTookPlaceEvent(LocalDateTime.of(2013, 1, 1, 0, 0)),
                EventsFixture.newEventTookPlaceEvent(eventId, LocalDateTime.of(2014, 1, 10, 0, 0)),
        ]

        when:
        events.each { e -> producer.publish(e) }
        def request = given()
        sleep(1000)

        then:
        def response = request.get("/api/v3/reports?start-date=2014-01-01&end-date=2014-12-31")


        then:
        response.statusCode == HttpStatus.OK.value()
        equalJson(response.getBody().asString()).matchesFile('json/single-event-report.json')
    }
}
