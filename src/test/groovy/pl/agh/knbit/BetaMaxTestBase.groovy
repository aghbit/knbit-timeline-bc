package pl.agh.knbit

import co.freeside.betamax.Recorder
import co.freeside.betamax.tape.yaml.OrderedPropertyComparator
import co.freeside.betamax.tape.yaml.TapePropertyUtils
import com.jayway.restassured.module.mockmvc.RestAssuredMockMvc
import com.sun.org.apache.xalan.internal.utils.XMLSecurityPropertyManager
import org.junit.Rule
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.context.WebApplicationContext

/**
 * @author Lukasz Raduj 2015 <raduj.lukasz at gmail.com>
 */
class BetaMaxTestBase extends SpringTestBase {

    @Rule
    Recorder recorder = new Recorder()

    @Autowired
    WebApplicationContext wac

    void setup() {
//        Betamax issue!
        TapePropertyUtils.metaClass.sort = { Set<XMLSecurityPropertyManager.Property> properties, List<String> names ->
            new LinkedHashSet(properties.sort(true, new OrderedPropertyComparator(names)))
        }

        RestAssuredMockMvc.webAppContextSetup(wac)
    }

    void cleanup() {
        semesterRepository.deleteAllInBatch()
    }
}
