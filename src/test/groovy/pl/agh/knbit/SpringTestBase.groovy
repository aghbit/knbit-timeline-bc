package pl.agh.knbit

import com.jayway.restassured.RestAssured
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.SpringApplicationConfiguration
import org.springframework.boot.test.WebIntegrationTest
import org.springframework.test.context.ActiveProfiles
import pl.agh.knbit.domain.semester.repository.SemesterRepository
import spock.lang.Shared
import spock.lang.Specification
import testdoubles.kafka.EmbeddedKafkaCluster
import testdoubles.kafka.EmbeddedZookeeper

/**
 * @author Lukasz Raduj 2015 <raduj.lukasz@gmail.com>
 */
@WebIntegrationTest
@ActiveProfiles('test')
@SpringApplicationConfiguration(classes = [TimelineBcAppContext.class])
class SpringTestBase extends Specification {
    final static int ZK_PORT = 2281
    final static int KAFKA_PORT = 9192

    @Value('${server.port}')
    int serverPort

    @Autowired
    SemesterRepository semesterRepository

    void setup() {
        RestAssured.port = serverPort
    }

    @Shared
    EmbeddedZookeeper embeddedZookeeper
    @Shared
    EmbeddedKafkaCluster embeddedKafkaCluster

    void setupSpec() {
        embeddedZookeeper = new EmbeddedZookeeper(ZK_PORT)
        def kafkaPorts = [KAFKA_PORT]
        embeddedKafkaCluster = new EmbeddedKafkaCluster(embeddedZookeeper.getConnection(),
                new Properties(), kafkaPorts)
        embeddedZookeeper.startup()
        embeddedKafkaCluster.startup()
        embeddedKafkaCluster.createTopics([
                'MemberJoinedKNBITEvent',
                'EventTookPlaceEvent',
                'TotalCommitsCountUpdateEvent',
                'SectionCreatedEvent',
                'MemberJoinedSectionEvent',
                'ProjectCreatedEvent',
                'MemberJoinedProjectEvent',
                'MemberLeftSectionEvent',
                'MemberLeftKNBITEvent'
        ])
    }

    void cleanupSpec() {
        embeddedKafkaCluster.shutdown()
        sleep(2000)
        embeddedZookeeper.shutdown()
    }

    void cleanup() {
        semesterRepository.deleteAllInBatch()
    }
}
