package pl.agh.knbit.spring

/**
 * @author Lukasz Raduj 2015 raduj.lukasz at gmail.com
 */
class ProdContextStartupTest extends SpringProdContextBase {

    def "Spring application context with prod profile should start without exception"() {
        expect:
        true
    }
}
