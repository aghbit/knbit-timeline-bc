package pl.agh.knbit.spring

import com.jayway.restassured.RestAssured
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.SpringApplicationConfiguration
import org.springframework.boot.test.WebIntegrationTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.TestPropertySource
import pl.agh.knbit.TimelineBcAppContext
import pl.agh.knbit.domain.semester.repository.SemesterRepository
import spock.lang.Shared
import spock.lang.Specification
import testdoubles.kafka.EmbeddedKafkaCluster
import testdoubles.kafka.EmbeddedZookeeper

/**
 * @author Lukasz Raduj 2015 <raduj.lukasz@gmail.com>
 */
@WebIntegrationTest
@ActiveProfiles('prod')
@TestPropertySource("classpath:outside-world-mocks.props")
@SpringApplicationConfiguration(classes = [
        TimelineBcAppContext.class
])
class SpringProdContextBase extends Specification {
    final static int ZK_PORT = 2281
    final static int KAFKA_PORT = 9192

    @Value('${server.port}')
    int serverPort

    @Autowired
    SemesterRepository periodRepository
    @Shared
    EmbeddedZookeeper embeddedZookeeper
    @Shared
    EmbeddedKafkaCluster embeddedKafkaCluster

    void setup() {
        RestAssured.port = serverPort
        periodRepository.deleteAllInBatch()
    }

    void setupSpec() {
        embeddedZookeeper = new EmbeddedZookeeper(ZK_PORT)
        def kafkaPorts = [KAFKA_PORT]
        embeddedKafkaCluster =
                new EmbeddedKafkaCluster(embeddedZookeeper.getConnection(), new Properties(), kafkaPorts)
        embeddedZookeeper.startup()
        embeddedKafkaCluster.startup()
        embeddedKafkaCluster.createTopics([
                'MemberJoinedKNBITEvent'
        ])
    }

    void cleanupSpec() {
        embeddedKafkaCluster.shutdown()
        embeddedZookeeper.shutdown()
    }
}
