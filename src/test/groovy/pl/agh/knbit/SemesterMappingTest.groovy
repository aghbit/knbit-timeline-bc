package pl.agh.knbit

import org.springframework.beans.factory.annotation.Autowired
import pl.agh.knbit.domain.semester.repository.SemesterRepository
import pl.agh.knbit.domain.semester.repository.TransactionalSemesterRepository

import static util.databuilder.SemesterFixture.newSemester

/**
 * @author Lukasz Raduj 2015 <raduj.lukasz at gmail.com>
 */
class SemesterMappingTest extends SpringTestBase {

    @Autowired
    SemesterRepository semesterRepository

    @Autowired
    TransactionalSemesterRepository transactionalSemesterRepository

    def "Should save and retrieved Semester instance without exception"() {
        given:
        def semester = newSemester()

        when:
        semesterRepository.save(semester)

        then: "persisted semester exists"
        def persistedSemester = transactionalSemesterRepository.findOneFullyLoaded(semester.semesterId())

        and: "has correct sections"
        persistedSemester.sections() == semester.sections()

        and: "has correct projects"
        persistedSemester.projects() == semester.projects()

        and: "has correct events"
        persistedSemester.events() == semester.events()
    }

    def "Should has only one semester after double save calls"() {
        given:
        def semester = newSemester()

        when:
        semesterRepository.save(semester)
        semesterRepository.save(semester)

        then: "no constraint validation exception was thrown"
        notThrown(Exception)

        and: "only one semester exists in repository"
        1L == semesterRepository.count()

        and: "persisted semester is correct"
        def persistedSemester = transactionalSemesterRepository.findOneFullyLoaded(semester.semesterId())
        persistedSemester == semester
    }

    def "Should have two semesters after saving two different entities"() {
        given:
        def firstSemester = newSemester()
        def secondSemester = newSemester()

        when:
        semesterRepository.save(firstSemester)
        semesterRepository.save(secondSemester)

        then:
        2L == semesterRepository.count()
    }

    def "Should be able to share same sections across Semesters"() {
        given: "two semesters with the same sections"
        def firstSemester = newSemester()
        def secondSemester = newSemester()
        secondSemester.sections(firstSemester.sections())

        when: "saving both semesters"
        semesterRepository.save(firstSemester)
        semesterRepository.save(secondSemester)

        then: "no constrain validation exception was thrown"
        notThrown(Exception)

        and: "semesters was correctly saved"
        firstSemester == transactionalSemesterRepository.findOneFullyLoaded(firstSemester.semesterId())
        secondSemester == transactionalSemesterRepository.findOneFullyLoaded(secondSemester.semesterId())
    }
}
