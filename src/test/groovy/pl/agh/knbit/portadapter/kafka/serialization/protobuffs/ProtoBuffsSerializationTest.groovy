package pl.agh.knbit.portadapter.kafka.serialization.protobuffs

import pl.agh.knbit.generated.protobuffs.MembersBc
import spock.lang.Specification
import util.databuilder.EventsFixture

/**
 * @author Lukasz Raduj 2015 raduj.lukasz at gmail.com
 */
class ProtoBuffsSerializationTest extends Specification {

    def "Should correctly serialize given message"() {
        given:
        def message = EventsFixture.newMemberJoinedKNBITEvent()

        when:
        byte[] bytes = ProtoBuffsEventSerializer.serialize(message)
        def deserializedMessage = ProtoBuffsEventDeserializer.deserialize(bytes, MembersBc.MemberJoinedKNBITEvent.class)

        then:
        deserializedMessage == message
    }
}
