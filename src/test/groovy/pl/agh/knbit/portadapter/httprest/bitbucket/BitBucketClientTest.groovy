package pl.agh.knbit.portadapter.httprest.bitbucket

import co.freeside.betamax.Betamax
import org.springframework.beans.factory.annotation.Autowired
import pl.agh.knbit.BetaMaxTestBase
import spock.lang.Ignore

/**
 * @author Lukasz Raduj 2015 raduj.lukasz at gmail.com
 */
class BitBucketClientTest extends BetaMaxTestBase {

    @Autowired
    BitBucketClient underTest

    @Ignore('betamax issue on jenkins, works locally')
    @Betamax(tape = "bitbucket")
    def "Should count all commits"() {
        when:

        def allCommits = underTest.countAllCommits()

        then:
        allCommits == 11205L
    }
}
