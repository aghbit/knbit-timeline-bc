package pl.agh.knbit.portadapter.httprest.adapters.springmvc

import com.jayway.restassured.module.mockmvc.RestAssuredMockMvc
import org.springframework.http.HttpStatus
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import pl.agh.knbit.domain.semester.repository.SemesterRepository
import spock.lang.Specification
import util.databuilder.SemesterFixture

import static com.jayway.restassured.module.mockmvc.RestAssuredMockMvc.given
import static util.config.PreConfiguredTestBeans.preConfiguredObjectMapper

/**
 * @author Lukasz Raduj 2015 raduj.lukasz at gmail.com
 */
class SemesterActivationControllerTest extends Specification {

    def periodRepositoryMock = Mock(SemesterRepository)

    void setup() {
        def controller = new SemesterActivationController(periodRepositoryMock)

        def mvcBuilder = MockMvcBuilders.standaloneSetup(controller)
                .setMessageConverters(new MappingJackson2HttpMessageConverter(preConfiguredObjectMapper()))
        RestAssuredMockMvc.standaloneSetup(mvcBuilder)
    }

    def "Should mark semester as non-draft after activation"() {
        given:
        def semester = SemesterFixture.newDraftSemester()
        def periodId = semester.semesterId()
        periodRepositoryMock.findOne(periodId) >> semester
        assert semester.isDraft()

        // @formatter:off
        when:
        given().
        when()
            .post("/api/v3/activate/${periodId.semesterId}").
        then()
            .statusCode(HttpStatus.OK.value())
            .headers(
                ['Content-Type':'application/json']
            )
        // @formatter:on

        then: 'semester is no longer a draft'
        !semester.isDraft()

        and: 'it was saved to db'
        1 * periodRepositoryMock.save(semester)
    }
}
