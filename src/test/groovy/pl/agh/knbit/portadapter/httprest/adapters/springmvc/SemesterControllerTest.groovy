package pl.agh.knbit.portadapter.httprest.adapters.springmvc

import co.freeside.betamax.Betamax
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import pl.agh.knbit.BetaMaxTestBase
import pl.agh.knbit.domain.semester.SemesterId
import spock.lang.Ignore
import util.databuilder.SemesterFixture

import static com.jayway.restassured.module.mockmvc.RestAssuredMockMvc.given
import static util.custommatcher.TimelineJsonMatcher.equalJson

/**
 * @author Lukasz Raduj 2015 <raduj.lukasz at gmail.com>
 */
class SemesterControllerTest extends BetaMaxTestBase {

    def "Should return all existing non-draft semesters ids when user has no admin permission"() {
        given:
        semesterRepository.save([SemesterFixture.newSemester(SemesterId.of('2010-SUMMER')),
                               SemesterFixture.newSemester(SemesterId.of('2013-WINTER')),
                               SemesterFixture.newDraftSemester()])

        def request = given()
//                no Authorization header

        when:
        def response = request.get("/api/v3/periods")

        then:
        response.statusCode == HttpStatus.OK.value()
        response.contentType == MediaType.APPLICATION_JSON_VALUE
        equalJson(response.body().asString()).matchesFile('json/non-draft-semesters-ids.json')
    }

    @Ignore('betamax issue on jenkins, works locally')
    @Betamax(tape = 'aa-bc-all-semesters')
    def "Should return all semesters (draft and non-draft) when user has read-draft permission"() {
        given:
        semesterRepository.save([SemesterFixture.newSemester(SemesterId.of('2012-SUMMER')),
                               SemesterFixture.newDraftSemester(SemesterId.of('2013-WINTER'))])

        def request = given()
                .header('knbit-aa-auth', '6olRH/Pm9LPOhR6I8jvOH+bf1Wp6Se8h')
        when:
        def response = request.get("/api/v3/periods")

        then:
        response.statusCode() == HttpStatus.OK.value()
        response.contentType == MediaType.APPLICATION_JSON_VALUE
        equalJson(response.body.asString()).matchesFile('json/all-semesters-ids.json')
    }

    def "Should load non-draft semester by id when user has no admin permission"() {
        given:
        def semester = SemesterFixture.fixedIdsSemester()
        semesterRepository.save(semester)
        def semesterId = semester.semesterId()

        def request = given()
//                no Authorization header

        when:
        def response = request.get("/api/v3/periods/${semesterId.getSemesterId()}")

        then:
        response.statusCode == HttpStatus.OK.value()
        equalJson(response.body.asString()).matchesFile('json/single-non-draft-semester.json')
        response.contentType == MediaType.APPLICATION_JSON_VALUE
    }

    def "Should return 404 status code when non-admin user asks for draft semester"() {
        given:
        def draftSemester = SemesterFixture.newDraftSemester()
        semesterRepository.save(draftSemester)
        def semesterId = draftSemester.semesterId()
        def request = given()
//                no Authorization header

        when:
        def response = request.get("/api/v3/periods/${semesterId.getSemesterId()}")

        then:
        response.statusCode == HttpStatus.NOT_FOUND.value()
    }

    @Ignore('betamax issue on jenkins, works locally')
    @Betamax(tape = 'aa-bc-load-draft-semester-has-permissions')
    def "Should return draft-semester when user with admin permission asks"() {
        given:
        def draftSemester = SemesterFixture.fixedIdsDraftSemester()
        semesterRepository.save(draftSemester)
        def semesterId = draftSemester.semesterId()
        def request = given()
                .header('knbit-aa-auth', '6olRH/Pm9LPOhR6I8jvOH+bf1Wp6Se8h')

        when:
        def response = request.get("/api/v3/periods/${semesterId.getSemesterId()}")

        then:
        response.statusCode == HttpStatus.OK.value()
        response.contentType == MediaType.APPLICATION_JSON_VALUE
        equalJson(response.body().asString()).matchesFile('json/single-draft-semester.json')
    }

    def "Should return 404 when any user asks for not existing semester"() {
        given:
        semesterRepository.deleteAllInBatch()
        def request = given()

        when:
        def response = request.get("/api/v3/periods/2098-WINTER")

        then:
        response.statusCode == HttpStatus.NOT_FOUND.value()
    }

    def "Should return last non-draft semester"() {
        given:
        def draftSemester = SemesterFixture.fixedIdsDraftSemester(SemesterId.of('2015-WINTER'))
        def secondSemester = SemesterFixture.fixedIdsSemesterWithoutEvents(SemesterId.of('2014-WINTER'))
        def thirdSemester = SemesterFixture.fixedIdsSemesterWithoutEvents(SemesterId.of('2014-SUMMER'))
        def fourthSemester = SemesterFixture.fixedIdsSemesterWithoutEvents(SemesterId.of('2013-WINTER'))
        semesterRepository.save(draftSemester)
        semesterRepository.save(secondSemester)
        semesterRepository.save(thirdSemester)
        semesterRepository.save(fourthSemester)

        def request = given()

        when:
        def response = request.get("/api/v3/periods/last")

        then:
        response.statusCode == HttpStatus.OK.value()
        response.contentType == MediaType.APPLICATION_JSON_VALUE
        equalJson(response.getBody().asString()).matchesFile('json/2014-summer.json')
    }

    def "Should return 404 when no semester exist"() {
        given:
        def request = given()

        when:
        def response = request.get("/api/v3/periods/last")

        then:
        response.statusCode == HttpStatus.NOT_FOUND.value()
    }
}
