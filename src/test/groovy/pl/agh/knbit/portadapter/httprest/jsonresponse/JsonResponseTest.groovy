package pl.agh.knbit.portadapter.httprest.jsonresponse

import pl.agh.knbit.domain.semester.SemesterId
import spock.lang.Shared
import spock.lang.Specification

import static org.skyscreamer.jsonassert.JSONAssert.assertEquals
import static util.config.PreConfiguredTestBeans.preConfiguredObjectMapper

/**
 * @author Lukasz Raduj 2015 <raduj.lukasz at gmail.com>
 */
class JsonResponseTest extends Specification {

    @Shared
    def mapper = preConfiguredObjectMapper()

    def "Should create correct JSON wrapper from collection of data"() {
        given:
        def ID_1 = '2012-WINTER'
        def ID_2 = '2013-SUMMER'
        def ID_3 = '2014-SUMMER'
        def data = [SemesterId.of(ID_1), SemesterId.of(ID_2), SemesterId.of(ID_3)]

        def expectedJson = """
                {
                   "wasSuccessful":true,
                   "data":[
                      {
                         "semesterId":"${ID_1}"
                      },
                      {
                         "semesterId":"${ID_2}"
                      },
                      {
                         "semesterId":"${ID_3}"
                      }
                   ],
                   "message":"Success"
                }
                """
        when:
        def producedJson = mapper.writeValueAsString(JsonResponse.ofSuccess(data))

        then:
        assertEquals(expectedJson, producedJson, true)
    }

    def "Should provide correct JSON representation of single data"() {
        given:
        def ID = '2012-SUMMER'
        def data = SemesterId.of(ID)

        def expectedJson = """
                    {
                        "wasSuccessful":true,
                        "data": {
                            "semesterId":"${ID}"
                        },
                        "message":"Success"
                    }
                    """
        when:
        def producedJson = mapper.writeValueAsString(JsonResponse.ofSuccess(data))

        then:
        assertEquals(expectedJson, producedJson, true)
    }
}
