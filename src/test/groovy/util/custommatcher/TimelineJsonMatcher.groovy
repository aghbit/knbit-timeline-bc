package util.custommatcher

import org.hamcrest.BaseMatcher
import org.hamcrest.Description
import org.skyscreamer.jsonassert.JSONAssert

/**
 * @author Lukasz Raduj 2015 <raduj.lukasz at gmail.com>
 */
class TimelineJsonMatcher extends BaseMatcher {

    private final String expectedJson

    private TimelineJsonMatcher(String expectedJson) {
        this.expectedJson = expectedJson
    }

    def static equalJson(String expectedJson) {
        new TimelineJsonMatcher(expectedJson)
    }

    @Override
    boolean matches(Object o) {
        if (!o instanceof String) {
            return false;
        }
        def givenString = (String) o

        try {
            JSONAssert.assertEquals(expectedJson, givenString, true)
        }
        catch (AssertionError ignored) {
            return false
        }

        true
    }

    boolean matchesFile(String jsonFilePath) {
        return matches(new File("src/test/resources/${jsonFilePath}").text)
    }

    @Override
    void describeMismatch(Object o, Description description) {

    }

    @Override
    void describeTo(Description description) {

    }
}
