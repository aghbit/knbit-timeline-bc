package util.custommatcher

import spock.lang.Specification
import spock.lang.Unroll

@Unroll
class TimelineJsonMatcherTest extends Specification {

    def "Should return true for the same JSONs and false for different"() {
        when:
        def matches = TimelineJsonMatcher.equalJson(firstJson).matches(secondJson)

        then:
        matches == expectedResult

        where:
        firstJson       | secondJson                        || expectedResult
        "{}"            | "{}"                              || true
        SOME_VALID_JSON | SOME_VALID_JSON                   || true
        SOME_VALID_JSON | SOME_VALID_JSON.replace('a', 'b') || false
        SOME_VALID_JSON | SOME_VALID_JSON_UNFORMATTED       || true
        SOME_VALID_JSON | SOME_VALID_JSON_WITH_EXTRA_FIELD  || false
    }

    def static String SOME_VALID_JSON = """
            {
               "menu":{
                  "id":"file",
                  "value":"File",
                  "popup":{
                     "menuitem":[
                        {
                           "value":"New",
                           "onclick":"CreateNewDoc()"
                        },
                        {
                           "value":"Open",
                           "onclick":"OpenDoc()"
                        },
                        {
                           "value":"Close",
                           "onclick":"CloseDoc()"
                        }
                     ]
                  }
               }
            }
    """
    def static String SOME_VALID_JSON_WITH_EXTRA_FIELD = """

            {
               "menu":{
                  "id":"file",
                  "value":"File",
                  "popup":{
                     "menuitem":[
                        {
                           "value":"New",
                           "onclick":"CreateNewDoc()"
                        },
                        {
                           "value":"Open",
                           "onclick":"OpenDoc()"
                        },
                        {
                           "value":"Close",
                           "onclick":"CloseDoc()"
                        },
                        {
                           "value":"Close",
                           "onclick":"CloseDoc()"
                        }
                     ]
                  }
               }
            }

    """

    def static String SOME_VALID_JSON_UNFORMATTED = """
            {"menu":{"id":"file","value":"File","popup":{"menuitem":[{"value":"New","onclick":"CreateNewDoc()"},{"value":"Open","onclick":"OpenDoc()"},{"value":"Close","onclick":"CloseDoc()"}]}}}
    """
}
