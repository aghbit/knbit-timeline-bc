package util.databuilder

import pl.agh.knbit.domain.semester.Member
import pl.agh.knbit.domain.semester.Semester
import pl.agh.knbit.domain.semester.SemesterId
import pl.agh.knbit.domain.semester.event.Event
import pl.agh.knbit.domain.semester.event.EventId
import pl.agh.knbit.domain.semester.event.EventType
import pl.agh.knbit.domain.semester.project.Project
import pl.agh.knbit.domain.semester.project.ProjectId
import pl.agh.knbit.domain.semester.project.ProjectState
import pl.agh.knbit.domain.semester.section.Section
import pl.agh.knbit.domain.semester.section.SectionId

/**
 * @author Lukasz Raduj 2015 <raduj.lukasz at gmail.com>
 */
class SemesterFixture {
    private static int nextYear = 2000

    def static Semester newSemester(SemesterId semesterId) {
        def semester = newDraftSemester(semesterId)
        semester.activate()

        semester
    }

    def static Semester newDraftSemester(SemesterId periodId) {
        def sectionId = SectionId.of(randomId())
        def eventId = EventId.of(randomId())
        Event event = new Event(eventId: eventId,
                description: "Will be awesome! Come all",
                sectionId: sectionId,
                eventType: EventType.LECTURE,
                title: "Apache Kafka introduction",
                pictureUrl: "http://hortonworks.com/wp-content/uploads/2014/08/kafka-logo-wide.png",
                speakers: [Member.of(randomId())] as Set)

        Set<Member> members = [Member.of(randomId()),
                                 Member.of(randomId()),
                                 Member.of(randomId())] as Set

        Project project = new Project(
                projectId: ProjectId.of(randomId()),
                projectState: ProjectState.IN_PROGRESS,
                name: "Get in Time!",
                description: "Time management system",
                sectionId: sectionId,
                participants: members,
                logoUrl: "http://knbit.edu.pl/media/storage/projects/logos/DLTLOGO_1.png")

        Section section = new Section(
                sectionId: sectionId,
                name: "Idea Factory",
                description: "Best projects are here",
                memberCount: 45)

        Semester semester = new Semester(periodId)

        semester.addEvents([event] as Set)
        semester.addProjects([project] as Set)
        semester.addSections([section] as Set)
        semester.addMembers(members)
        semester.increaseCommitsCount(2199)

        semester
    }

    def static Semester newDraftSemester() {
        def year = nextYear++
        def semesterId = SemesterId.of("${year}-SUMMER")
        newDraftSemester(semesterId)
    }

    def static Semester newSemester() {
        def semester = newDraftSemester()
        semester.activate()

        semester
    }

    def static Semester fixedIdsSemester() {
        def semester = fixedIdsDraftSemester()
        semester.activate()

        semester
    }

    def static Semester fixedIdsSemester(SemesterId semesterId) {
        def semester = fixedIdsDraftSemester(semesterId)
        semester.activate()

        semester
    }

    def static Semester fixedIdsSemesterWithoutEvents(SemesterId semesterId) {
        def semester = fixedIdsSemester(semesterId)
        semester.events().clear()

        semester
    }

    def static Semester fixedIdsDraftSemester() {
        fixedIdsDraftSemester(SemesterId.of('2015-WINTER'))
    }


    def static Semester fixedIdsDraftSemester(SemesterId semesterId) {
        def sectionId = SectionId.of('4dfe592e-e5c0-4034-b573-95ff38fe5fbb')
        def eventId = EventId.of('d8863031-547e-460a-9c3d-82025dfc5584')
        Event event = new Event(eventId: eventId,
                description: "Will be awesome! Come all",
                sectionId: sectionId,
                eventType: EventType.LECTURE,
                pictureUrl: "http://hortonworks.com/wp-content/uploads/2014/08/kafka-logo-wide.png",
                title: "Apache Kafka introduction",
                speakers: [Member.of('be6f00f7-bac2-4a84-8941-d5917acd2398')] as Set)

        Set<Member> members = [
                Member.of('be6f00f7-bac2-4a84-8941-d5917acd23e2'),
                Member.of('d2039a4d-498f-48f4-9092-d5a21a1d4325'),
                Member.of('f72e5b50-3e80-4b9a-8e6d-afd759d9ad88')
        ] as Set

        Project project = new Project(
                projectId: ProjectId.of('3091c0c6-6032-415b-af90-f39e205a4d12'),
                projectState: ProjectState.IN_PROGRESS,
                name: "Get in Time!",
                description: "Time management system",
                sectionId: sectionId,
                participants: members,
                logoUrl: "http://knbit.edu.pl/media/storage/projects/logos/DLTLOGO_1.png")

        Section section = new Section(
                sectionId: sectionId,
                name: "Idea Factory",
                description: "Best projects are here",
                memberCount: 45)

        Semester semester = new Semester(semesterId)

        semester.addEvents([event] as Set)
        semester.addProjects([project] as Set)
        semester.addSections([section] as Set)
        semester.addMembers(members)
        semester.increaseCommitsCount(2134)

        semester
    }

    def static String randomId() {
        UUID.randomUUID().toString()
    }

    def static Semester currentSemester() {
        def semester = newSemester()
        semester.semesterId(SemesterId.ofCurrentSemester())

        semester
    }
}
