package util.databuilder

import pl.agh.knbit.domain.semester.section.Section
import pl.agh.knbit.domain.semester.section.SectionId

class SectionFixture {
    static def Section newSection(SectionId sectionId) {
        Section.builder()
                .sectionId(sectionId)
                .name('BIT Algo')
                .description('Smartest guys here')
                .logoUrl('http://trololo-url')
                .build()
    }

    static def nextSectionId() {
        SectionId.of(UUID.randomUUID().toString())
    }
}
