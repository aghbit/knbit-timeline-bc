package util.databuilder

import pl.agh.knbit.domain.feeds.Feed

import java.time.LocalDateTime
import java.time.ZoneOffset

class FeedFixture {

    def static newFeed(LocalDateTime occurrenceDateTime) {
        new Feed('John Crosby joined KN BIT. Welcome!', Date.from(occurrenceDateTime.toInstant(ZoneOffset.UTC)), 'MemberJoinedKNBITEvent')
    }

    def static newMemberLeftKNBITFeed(LocalDateTime occurrenceDateTime) {
        new Feed('Member John Crosby left KN BIT. Reason NOT-PROVIDED', Date.from(occurrenceDateTime.toInstant(ZoneOffset.UTC)), 'MemberLeftKNBITEvent')
    }
}
