package util.databuilder

import pl.agh.knbit.domain.semester.Member
import pl.agh.knbit.domain.semester.event.EventId
import pl.agh.knbit.domain.semester.project.ProjectId
import pl.agh.knbit.domain.semester.section.SectionId
import pl.agh.knbit.generated.protobuffs.EventsBc
import pl.agh.knbit.generated.protobuffs.MembersBc
import pl.agh.knbit.generated.protobuffs.ProjectsBc
import pl.agh.knbit.generated.protobuffs.SectionsBc
import pl.agh.knbit.util.DateTimeUtils

import java.time.LocalDateTime

/**
 * @author Lukasz Raduj 2015 raduj.lukasz at gmail.com
 */
class EventsFixture {

    static def MembersBc.MemberJoinedKNBITEvent newMemberJoinedKNBITEvent(String id, LocalDateTime eventDate) {
        MembersBc.MemberJoinedKNBITEvent
                .newBuilder()
                .setMemberId(id)
                .setUtcDateAsEpochSeconds(DateTimeUtils.toEpochSeconds(eventDate))
                .setMemberFirstName('Mark')
                .setMemberLastName('Knopfler')
                .setMemberEmail('mark@direstraits.com')
                .build()
    }

    static def MembersBc.MemberJoinedKNBITEvent newMemberJoinedKNBITEvent() {
        newMemberJoinedKNBITEvent(uuid(), LocalDateTime.now())
    }

    static def MembersBc.MemberJoinedKNBITEvent newMemberJoinedKNBITEvent(String memberId) {
        newMemberJoinedKNBITEvent(memberId, LocalDateTime.now())
    }

    static def EventsBc.EventTookPlaceEvent newEventTookPlaceEvent(EventId eventId, LocalDateTime eventDate) {
        def speaker = EventsBc.EventTookPlaceEvent.Speaker
                .newBuilder()
                .setName('Bartek Szczepanik')
                .setAccountId('bartek-id')
                .build()

        EventsBc.EventTookPlaceEvent
                .newBuilder()
                .setUtcDateAsEpochSeconds(DateTimeUtils.toEpochSeconds(eventDate))
                .setEventId(eventId.eventId)
                .setEventName('Twitter Zipkin Intro!')
                .setEventDescription('Awesome event')
                .setAttendesCount(50)
                .setEventType(EventsBc.EventTookPlaceEvent.EventType.LECTURE)
                .setSectionId('if-id')
                .addAllSpeakers([speaker])
                .setEventPictureUrl('http://4.bp.blogspot.com/-DsmFcaeMi4U/T9DX_XLMrdI/AAAAAAAAABg/jKF_pXToIEo/s1600/zipkin.jpg')
                .build()
    }

    static def EventsBc.EventTookPlaceEvent newEventTookPlaceEvent() {
        newEventTookPlaceEvent(EventId.of(uuid()), LocalDateTime.now())
    }

    static def EventsBc.EventTookPlaceEvent newEventTookPlaceEvent(EventId eventId) {
        newEventTookPlaceEvent(eventId, LocalDateTime.now())
    }

    static def EventsBc.EventTookPlaceEvent newEventTookPlaceEvent(LocalDateTime eventDate) {
        newEventTookPlaceEvent(EventId.of(uuid()), eventDate)
    }

    def static newSectionCreatedEvent(SectionId sectionId) {
        SectionsBc.SectionCreatedEvent
                .newBuilder()
                .setUtcDateAsEpochSeconds(DateTimeUtils.nowAsEpochSeconds())
                .setSectionId(sectionId.sectionId)
                .setSectionName('Idea Factory')
                .setSectionDescription('The best section')
                .setLogoUrl('http://4.bp.blogspot.com/-DsmFcaeMi4U/T9DX_XLMrdI/AAAAAAAAABg/jKF_pXToIEo/s1600/zipkin.jpg')
                .build();
    }

    def static newSectionCreatedEvent() {
        newSectionCreatedEvent(SectionId.of(uuid()))
    }

    def static newMemberJoinedSectionEvent(SectionId sectionId) {
        SectionsBc.MemberJoinedSectionEvent
                .newBuilder()
                .setUtcDateAsEpochSeconds(DateTimeUtils.nowAsEpochSeconds())
                .setMemberFirstName('Eric')
                .setMemberLastName('Clapton')
                .setSectionId(sectionId.sectionId)
                .setSectionName('Idea factory')
                .setMemeberId(uuid())
                .build()
    }

    static def newMemberLeftSectionEvent(SectionId sectionId) {
        newMemberLeftSectionEvent(sectionId, uuid())
    }

    static def newMemberLeftSectionEvent(SectionId sectionId, String memberId) {
        SectionsBc.MemberLeftSectionEvent
                .newBuilder()
                .setUtcDateAsEpochSeconds(DateTimeUtils.nowAsEpochSeconds())
                .setSectionId(sectionId.sectionId)
                .setMemeberId(memberId)
                .setReason('No time left :/')
                .build()
    }

    def static String uuid() {
        UUID.randomUUID().toString()
    }

    def static newProjectCreatedEvent() {
        newProjectCreatedEvent(ProjectId.of(uuid()))
    }

    def static newProjectCreatedEvent(ProjectId projectId) {
        ProjectsBc.ProjectCreatedEvent
                .newBuilder()
                .setUtcDateAsEpochSeconds(DateTimeUtils.nowAsEpochSeconds())
                .setProjectId(projectId.projectId)
                .setProjectName('Get In Time')
                .setProjectDescription('System for managing time in Get In Bank department')
                .setSectionId('00a6ef19-c701-4a14-99e5-dcdec7f1d66f')
                .setLogoUrl('https://scontent-waw1-1.xx.fbcdn.net/hprofile-xfp1/v/t1.0-1/p320x320/10370815_1535992229996129_8764031255517120497_n.png?oh=dfa6ee204c63097f9f09283b3f45821c&oe=56EFB58F')
                .setClientName('Get in Bank')
                .build()
    }

    def static newMemberJoinedProjectEvent(ProjectId projectId, String memberId) {
        ProjectsBc.MemberJoinedProjectEvent
                .newBuilder()
                .setUtcDateAsEpochSeconds(DateTimeUtils.nowAsEpochSeconds())
                .setProjectId(projectId.projectId)
                .setMemberFirstName('Mark')
                .setMemberLastName('Knopfler')
                .setMemberId(memberId)
                .setProjectName('Decompiler')
                .build()
    }

    def static newMemberLeftKNBITEvent() {
        newMemberLeftKNBITEvent(LocalDateTime.now(), Member.of(uuid()))
    }

    def static newMemberLeftKNBITEvent(LocalDateTime dateTime, Member memberId) {
        MembersBc.MemberLeftKNBITEvent.newBuilder()
                .setUtcDateAsEpochSeconds(DateTimeUtils.toEpochSeconds(dateTime))
                .setMemberFirstName("Janusz")
                .setMemberLastName("Bober")
                .setMemberId(memberId.memberId())
                .build()
    }
}
