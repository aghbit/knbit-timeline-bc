package util.config

import com.fasterxml.jackson.annotation.JsonAutoDetect
import com.fasterxml.jackson.databind.ObjectMapper

/**
 * @author Lukasz Raduj 2015 <raduj.lukasz at gmail.com>
 */
class PreConfiguredTestBeans {
    def static preConfiguredObjectMapper() {
        def mapper = new ObjectMapper()
        mapper.setVisibilityChecker(mapper.getSerializationConfig().getDefaultVisibilityChecker()
                .withFieldVisibility(JsonAutoDetect.Visibility.ANY)
                .withGetterVisibility(JsonAutoDetect.Visibility.NONE)
                .withSetterVisibility(JsonAutoDetect.Visibility.NONE)
                .withCreatorVisibility(JsonAutoDetect.Visibility.NONE))
        mapper
    }

}
