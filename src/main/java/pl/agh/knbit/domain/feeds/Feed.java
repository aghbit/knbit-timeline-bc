package pl.agh.knbit.domain.feeds;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.*;
import java.time.ZoneOffset;
import java.util.Date;

import static pl.agh.knbit.util.DateTimeUtils.fromEpochSeconds;

/**
 * @author Lukasz Raduj 2015 raduj.lukasz at gmail.com
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Table(name = "feed")
public class Feed extends AbstractPersistable<Long> {

    @Column(name = "message", length = 2000)
    private String message;

    @Column(name = "occurrence_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date occurrenceDate;

    @Column(name = "source_event")
    private String sourceEvent;

    public Feed(String message, long occurrenceDateAsUtcEpochSeconds, String sourceEvent) {
        this(message, Date.from(fromEpochSeconds(occurrenceDateAsUtcEpochSeconds).toInstant(ZoneOffset.UTC)), sourceEvent);
    }
}
