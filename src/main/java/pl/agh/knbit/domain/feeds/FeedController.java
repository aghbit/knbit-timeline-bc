package pl.agh.knbit.domain.feeds;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.agh.knbit.portadapter.httprest.aabc.Authorized;
import pl.agh.knbit.portadapter.httprest.aabc.Permission;
import pl.agh.knbit.portadapter.httprest.adapters.springmvc.Principal;
import pl.agh.knbit.portadapter.httprest.jsonresponse.JsonResponse;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/api/v3", produces = MediaType.APPLICATION_JSON_VALUE)
public class FeedController {
    private static final int PAGE_SIZE = 20;
    private final FeedRepository feedRepository;

    @Value("#{'${feeds.allowed-feeds}'.split(',')}")
    private List<String> publicFeeds;

    @Autowired
    public FeedController(FeedRepository feedRepository) {
        this.feedRepository = feedRepository;
    }

    @Authorized(permission = Permission.READ_FULL_EVENTS_FEED)
    @RequestMapping(value = "/feeds", method = RequestMethod.GET)
    public JsonResponse loadFeeds(@RequestParam(value = "page", required = false) Optional<Integer> page, Principal principal) {

        Page<Feed> feedsPage;
        if (principal.hasRequiredPermissions()) {
            feedsPage = feedRepository.findAll(buildPageRequest(page));
        } else {
            feedsPage = feedRepository.findBySourceEventIn(publicFeeds, buildPageRequest(page));
        }
        return JsonResponse.ofSuccess(feedsPage);
    }

    private PageRequest buildPageRequest(Optional<Integer> page) {
        return new PageRequest(page.orElse(0), PAGE_SIZE, Sort.Direction.DESC, "occurrenceDate");
    }
}
