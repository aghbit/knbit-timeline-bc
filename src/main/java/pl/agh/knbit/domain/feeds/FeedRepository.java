package pl.agh.knbit.domain.feeds;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface FeedRepository extends JpaRepository<Feed, Long> {

    Page<Feed> findBySourceEventIn(Collection<String> publicFeeds, Pageable pageable);
}
