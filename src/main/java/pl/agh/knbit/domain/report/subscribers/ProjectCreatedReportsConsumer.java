package pl.agh.knbit.domain.report.subscribers;

import pl.agh.knbit.domain.report.Report;
import pl.agh.knbit.domain.report.ReportQuery;
import pl.agh.knbit.domain.report.ReportsEventConsumer;
import pl.agh.knbit.domain.semester.project.Project;
import pl.agh.knbit.generated.protobuffs.ProjectsBc;
import pl.agh.knbit.portadapter.kafka.serialization.protobuffs.ProtoBuffsEventDeserializer;

import java.util.Objects;
import java.util.function.Consumer;

public class ProjectCreatedReportsConsumer implements ReportsEventConsumer {
    @Override
    public String commitLogTopic() {
        return "ProjectCreatedEvent";
    }

    @Override
    public Consumer<byte[]> eventConsumer(Report report, ReportQuery query) {
        return (bytes) -> {
            ProjectsBc.ProjectCreatedEvent event =
                    ProtoBuffsEventDeserializer.deserialize(bytes, ProjectsBc.ProjectCreatedEvent.class);

            if (Objects.isNull(event)) {
                return;
            }

            if (query.wasEventBeforeQueryEndDate(event.getUtcDateAsEpochSeconds())) {
                Project project = Project.fromEvent(event);
                report.addProject(project);
            }
        };
    }
}
