package pl.agh.knbit.domain.report.replacers;

import pl.agh.knbit.domain.report.Report;
import pl.agh.knbit.domain.semester.event.Event;
import pl.agh.knbit.domain.semester.event.EventType;
import pl.agh.knbit.domain.semester.project.Project;
import pl.agh.knbit.domain.semester.section.Section;
import pl.agh.knbit.domain.semester.section.SectionId;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

public class ProjectsAndEventsReplacer implements IReplacer {

    @Override
    public String token() {
        return "${projects-and-events-grouped-by-sections}";
    }

    @Override
    public String apply(String input, Report reportData) {
        return input.replace(token(), buildProjectsAnsEventGroupedBySections(reportData));
    }

    private static String buildProjectsAnsEventGroupedBySections(Report reportData) {
        Map<SectionId, Section> sectionMap = reportData.sections()
                .stream()
                .collect(toMap(Section::sectionId, section -> section));

        Map<SectionId, ProjectAndEvents> projectAndEventsMap = reportData.sections()
                .stream()
                .collect(toMap(Section::sectionId, section -> new ProjectAndEvents()));

        reportData.projects()
                .forEach(project -> {
                    if (!projectAndEventsMap.containsKey(project.sectionId())) {
                        return;
                    }
                    projectAndEventsMap.get(project.sectionId()).projects.add(project);
                });

        reportData.events()
                .forEach(event -> {
                    if (!projectAndEventsMap.containsKey(event.sectionId())) {
                        return;
                    }
                    projectAndEventsMap.get(event.sectionId()).events.add(event);
                });

        StringBuilder builder = new StringBuilder();
        projectAndEventsMap.entrySet()
                .stream()
                .filter(entry -> entry.getValue().hasData())
                .forEach(entry -> {
                    Section section = sectionMap.get(entry.getKey());

                    builder.append("Sekcja: ")
                            .append(section.name().toUpperCase())
                            .append("\n")
                            .append(entry.getValue().toString())
                            .append("\n\n");
                });

        return builder.toString();
    }

    private final static class ProjectAndEvents {
        Set<Project> projects = new HashSet<>();
        Set<Event> events = new HashSet<>();

        public boolean hasData() {
            return !projects.isEmpty() || !events.isEmpty();
        }

        @Override
        public String toString() {
            StringBuilder builder = new StringBuilder();

            List<Event> lectures = events.stream()
                    .filter(event -> event.eventType() == EventType.LECTURE)
                    .collect(toList());

            if (!lectures.isEmpty()) {
                builder.append("Przeprowadzone wykłady:\n");
                lectures.forEach(lecture -> {
                    builder.append(lecture.title());
                    builder.append("\n");
                });
                builder.append("\n");
            }

            List<Event> workshops = events.stream()
                    .filter(event -> event.eventType() == EventType.WORKSHOP)
                    .collect(toList());

            if (!workshops.isEmpty()) {
                builder.append("Przeprowadzone warsztaty:\n");
                workshops.forEach(workshop -> {
                    builder.append(workshop.title());
                    builder.append("\n");
                });
                builder.append("\n");

            }
            if (!projects.isEmpty()) {
                builder.append("Prowadzone projekty:\n");
                projects.forEach(project -> {
                    builder.append(project.name())
                            .append("\n")
                            .append(project.description())
                            .append("\n")
                            .append("Wielkość zespołu: ")
                            .append(project.participants().size())
                            .append("\n");
                });
                builder.append("\n");
            }

            return builder.toString();
        }
    }
}
