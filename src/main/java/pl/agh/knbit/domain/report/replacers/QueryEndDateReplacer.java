package pl.agh.knbit.domain.report.replacers;

import pl.agh.knbit.domain.report.Report;

public class QueryEndDateReplacer implements IReplacer {
    @Override
    public String token() {
        return "${query-end-date}";
    }

    @Override
    public String apply(String input, Report reportData) {

        return input.replace(token(), reportData.query().endDate().toString());
    }
}
