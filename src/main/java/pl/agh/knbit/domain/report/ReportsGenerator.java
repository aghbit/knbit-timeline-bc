package pl.agh.knbit.domain.report;

import com.google.common.collect.ImmutableList;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import pl.agh.knbit.domain.report.subscribers.*;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Slf4j
@Component
public class ReportsGenerator {

    @Value("${endpoints.kafka.zookeeper.connect}")
    private String zookeeperConnect;

    private KafkaLowLevelConsumer consumer;
    private final List<ReportsEventConsumer> subscribers = ImmutableList.<ReportsEventConsumer>builder()
            .add(new MemberJoinedKNBITReportsConsumer())
            .add(new MemberLeftKNBITReportsConsumer())
            .add(new SectionCreatedReportsConsumer())
            .add(new MemberJoinedSectionReportsConsumer())
            .add(new MemberLeftSectionReportsConsumer())
            .add(new ProjectCreatedReportsConsumer())
            .add(new MemberJoinedProjectReportsConsumer())
            .add(new EventTookPlaceReportsConsumer())
            .build();

    private final ExecutorService executorService = Executors.newFixedThreadPool(1);

    @Autowired
    public ReportsGenerator(KafkaLowLevelConsumer consumer) {
        this.consumer = consumer;
    }

    public Report generate(ReportQuery query) {
        Report report = new Report(query);

        subscribers.forEach(s -> consumer.readAllExistingMessages(s.eventConsumer(report, query), s.commitLogTopic()));

        return report;
    }
}
