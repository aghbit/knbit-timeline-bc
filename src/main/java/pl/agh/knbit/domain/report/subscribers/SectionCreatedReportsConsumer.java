package pl.agh.knbit.domain.report.subscribers;

import pl.agh.knbit.domain.report.Report;
import pl.agh.knbit.domain.report.ReportQuery;
import pl.agh.knbit.domain.report.ReportsEventConsumer;
import pl.agh.knbit.domain.semester.section.Section;
import pl.agh.knbit.generated.protobuffs.SectionsBc;
import pl.agh.knbit.portadapter.kafka.serialization.protobuffs.ProtoBuffsEventDeserializer;

import java.util.Objects;
import java.util.function.Consumer;

public class SectionCreatedReportsConsumer implements ReportsEventConsumer {
    @Override
    public String commitLogTopic() {
        return "SectionCreatedEvent";
    }

    @Override
    public Consumer<byte[]> eventConsumer(Report report, ReportQuery query) {
        return (e) -> {
            SectionsBc.SectionCreatedEvent event =
                    ProtoBuffsEventDeserializer.deserialize(e, SectionsBc.SectionCreatedEvent.class);

            if (Objects.isNull(event)) {
                return;
            }

            if(query.wasEventBeforeQueryEndDate(event.getUtcDateAsEpochSeconds())) {
                Section section = Section.fromEvent(event);
                report.addSection(section);
            }
        };
    }
}
