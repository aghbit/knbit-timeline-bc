package pl.agh.knbit.domain.report;

import java.util.function.Consumer;

/**
 * @author Lukasz Raduj 2015 raduj.lukasz at gmail.com
 */
public interface ReportsEventConsumer {
    String commitLogTopic();

    Consumer<byte[]> eventConsumer(Report report, ReportQuery query);
}
