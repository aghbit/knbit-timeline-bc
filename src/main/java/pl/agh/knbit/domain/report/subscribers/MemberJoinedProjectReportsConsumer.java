package pl.agh.knbit.domain.report.subscribers;

import pl.agh.knbit.domain.report.Report;
import pl.agh.knbit.domain.report.ReportQuery;
import pl.agh.knbit.domain.report.ReportsEventConsumer;
import pl.agh.knbit.domain.semester.Member;
import pl.agh.knbit.generated.protobuffs.ProjectsBc;
import pl.agh.knbit.portadapter.kafka.serialization.protobuffs.ProtoBuffsEventDeserializer;

import java.util.Objects;
import java.util.function.Consumer;

public class MemberJoinedProjectReportsConsumer implements ReportsEventConsumer {
    @Override
    public String commitLogTopic() {
        return "MemberJoinedProjectEvent";
    }

    @Override
    public Consumer<byte[]> eventConsumer(Report report, ReportQuery query) {
        return (bytes) -> {
            ProjectsBc.MemberJoinedProjectEvent event =
                    ProtoBuffsEventDeserializer.deserialize(bytes, ProjectsBc.MemberJoinedProjectEvent.class);

            if (Objects.isNull(event)) {
                return;
            }

            if (query.wasEventBeforeQueryEndDate(event.getUtcDateAsEpochSeconds())) {
                report.projects()
                        .stream()
                        .filter(p -> p.projectId().getProjectId().equals(event.getProjectId()))
                        .findFirst()
                        .ifPresent((p) -> p.addParticipant(Member.of(event.getMemberId())));
            }
        };
    }
}
