package pl.agh.knbit.domain.report.replacers;

import pl.agh.knbit.domain.report.Report;

public interface IReplacer {

    String token();

    String apply(String input, Report reportData);
}
