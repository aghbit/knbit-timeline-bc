package pl.agh.knbit.domain.report;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import pl.agh.knbit.util.DateTimeUtils;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
@Accessors(fluent = true)
@NoArgsConstructor
@AllArgsConstructor
public class ReportQuery {
    public static final String DATE_FORMAT = "yyyy-MM-dd";

    @NotNull
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate startDate;

    @NotNull
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate endDate;

    public boolean wasEventBetweenQueryDates(long utcDateAsEpochSeconds) {
        LocalDate eventDate = DateTimeUtils.fromEpochSeconds(utcDateAsEpochSeconds).toLocalDate();

        return startDate.isBefore(eventDate) && endDate.isAfter(eventDate);
    }

    public boolean wasEventBeforeQueryEndDate(long utcDateAsEpochSeconds) {
        LocalDate eventDate = DateTimeUtils.fromEpochSeconds(utcDateAsEpochSeconds).toLocalDate();

        return endDate.isAfter(eventDate);
    }
}
