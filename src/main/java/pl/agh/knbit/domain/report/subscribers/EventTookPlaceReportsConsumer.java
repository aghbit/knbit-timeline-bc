package pl.agh.knbit.domain.report.subscribers;

import lombok.extern.slf4j.Slf4j;
import pl.agh.knbit.domain.report.Report;
import pl.agh.knbit.domain.report.ReportQuery;
import pl.agh.knbit.domain.report.ReportsEventConsumer;
import pl.agh.knbit.domain.semester.event.Event;
import pl.agh.knbit.generated.protobuffs.EventsBc;
import pl.agh.knbit.portadapter.kafka.serialization.protobuffs.ProtoBuffsEventDeserializer;

import java.util.Objects;
import java.util.function.Consumer;

@Slf4j
public class EventTookPlaceReportsConsumer implements ReportsEventConsumer {

    @Override
    public String commitLogTopic() {
        return "EventTookPlaceEvent";
    }

    @Override
    public Consumer<byte[]> eventConsumer(Report report, ReportQuery query) {
        return (bytes) -> {
            log.debug("Processing EventTookPlace event for report generation");
            EventsBc.EventTookPlaceEvent event =
                    ProtoBuffsEventDeserializer.deserialize(bytes, EventsBc.EventTookPlaceEvent.class);

            if (Objects.isNull(event)) {
                return;
            }

            if (query.wasEventBetweenQueryDates(event.getUtcDateAsEpochSeconds())) {
                Event ev = Event.fromEvent(event);
                report.addEvent(ev);
            }
        };
    }
}
