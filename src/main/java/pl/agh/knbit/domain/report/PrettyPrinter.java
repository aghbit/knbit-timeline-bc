package pl.agh.knbit.domain.report;

import com.google.common.collect.ImmutableSet;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import pl.agh.knbit.domain.report.replacers.*;

import java.io.IOException;
import java.io.InputStream;

@Slf4j
public class PrettyPrinter {

    private final static ImmutableSet<IReplacer> replacers = new ImmutableSet.Builder<IReplacer>()
            .add(new CurrentDateReplacer())
            .add(new MembersCountReplacer())
            .add(new MembersLeftCountReplacer())
            .add(new QueryEndDateReplacer())
            .add(new QueryStartDateReplacer())
            .add(new SectionsReplacer())
            .add(new ProjectsAndEventsReplacer())
            .add(new EventsReplacer())
            .build();

    public static String prettyfiy(Report reportData) {
        StringBuilder report = new StringBuilder(readFromFile("/reports/reports-template.txt"));

        report.append("\n");
        report.append("\n");

        String tmp = report.toString();
        for (IReplacer replacer : replacers) {
            tmp = replacer.apply(tmp, reportData);
        }


        return tmp;
    }

    private static String readFromFile(String path) {
        try (InputStream stream = PrettyPrinter.class.getResourceAsStream(path)) {
            return IOUtils.toString(stream);
        } catch (IOException e) {
            log.error("Cannot read file");
            return "";
        }
    }
}
