package pl.agh.knbit.domain.report.replacers;

import pl.agh.knbit.domain.report.Report;

import java.time.LocalDate;

public class CurrentDateReplacer implements IReplacer {
    @Override
    public String token() {
        return "${current-date}";
    }

    @Override
    public String apply(String input, Report ignored) {
        LocalDate now = LocalDate.now();

        return input.replace(token(), now.toString());
    }
}
