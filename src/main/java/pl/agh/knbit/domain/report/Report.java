package pl.agh.knbit.domain.report;

import lombok.Data;
import lombok.experimental.Accessors;
import pl.agh.knbit.domain.semester.Member;
import pl.agh.knbit.domain.semester.event.Event;
import pl.agh.knbit.domain.semester.project.Project;
import pl.agh.knbit.domain.semester.section.Section;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Lukasz Raduj 2015 raduj.lukasz at gmail.com
 */
@Data
@Accessors(fluent = true)
public class Report {
    private final ReportQuery query;

    private final Set<Member> members = new HashSet<>();
    private final Set<Member> leftMembers = new HashSet<>();
    private final Set<Section> sections = new HashSet<>();
    private final Set<Project> projects = new HashSet<>();
    private final Set<Event> events = new HashSet<>();

    public Report(ReportQuery query) {
        this.query = query;
    }

    public Report addMember(Member member) {
        members.add(member);

        return this;
    }

    public Report removeMember(Member member) {
        leftMembers.add(member);

        return this;
    }

    public Report addSection(Section section) {
        sections.add(section);

        return this;
    }

    public Report addProject(Project project) {
        projects.add(project);

        return this;
    }

    public Report addEvent(Event event) {
        events.add(event);

        return this;
    }
}
