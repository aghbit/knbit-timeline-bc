package pl.agh.knbit.domain.report.subscribers;

import pl.agh.knbit.domain.report.Report;
import pl.agh.knbit.domain.report.ReportQuery;
import pl.agh.knbit.domain.report.ReportsEventConsumer;
import pl.agh.knbit.domain.semester.Member;
import pl.agh.knbit.generated.protobuffs.MembersBc;
import pl.agh.knbit.portadapter.kafka.serialization.protobuffs.ProtoBuffsEventDeserializer;

import java.util.Objects;
import java.util.function.Consumer;

public class MemberJoinedKNBITReportsConsumer implements ReportsEventConsumer {

    @Override
    public String commitLogTopic() {
        return "MemberJoinedKNBITEvent";
    }

    @Override
    public Consumer<byte[]> eventConsumer(Report report, ReportQuery query) {
        return (bytes) -> {
            MembersBc.MemberJoinedKNBITEvent event =
                    ProtoBuffsEventDeserializer.deserialize(bytes, MembersBc.MemberJoinedKNBITEvent.class);

            //todo: extract
            if (Objects.isNull(event)) {
                return;
            }

            if(query.wasEventBeforeQueryEndDate(event.getUtcDateAsEpochSeconds())) {
                report.addMember(Member.of(event.getMemberId()));
            }
        };
    }
}
