package pl.agh.knbit.domain.report;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.agh.knbit.portadapter.httprest.jsonresponse.JsonResponse;

import java.time.LocalDate;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.TEXT_PLAIN_VALUE;
import static pl.agh.knbit.domain.report.ReportQuery.DATE_FORMAT;

@RestController
@RequestMapping(value = "/api/v3")
public class ReportsController {

    private final ReportsGenerator reportsGenerator;

    @Autowired
    public ReportsController(ReportsGenerator reportsGenerator) {
        this.reportsGenerator = reportsGenerator;
    }

    @RequestMapping(value = "/reports", method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE)
    public JsonResponse<Report> generateReport(@RequestParam("start-date")
                                               @DateTimeFormat(pattern = DATE_FORMAT) LocalDate startDate,
                                               @RequestParam("end-date")
                                               @DateTimeFormat(pattern = DATE_FORMAT) LocalDate endDate) {

        ReportQuery query = new ReportQuery(startDate, endDate);
        Report report = reportsGenerator.generate(query);

        return JsonResponse.ofSuccess(report);
    }

    @RequestMapping(value = "/reports/pretty", method = RequestMethod.GET, produces = TEXT_PLAIN_VALUE)
    public String generatePrettyReport(@RequestParam("start-date")
                                               @DateTimeFormat(pattern = DATE_FORMAT) LocalDate startDate,
                                               @RequestParam("end-date")
                                               @DateTimeFormat(pattern = DATE_FORMAT) LocalDate endDate) {

        ReportQuery query = new ReportQuery(startDate, endDate);
        Report report = reportsGenerator.generate(query);
        String prettyReport = PrettyPrinter.prettyfiy(report);

        return prettyReport;
    }
}
