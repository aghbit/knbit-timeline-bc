package pl.agh.knbit.domain.report.replacers;

import pl.agh.knbit.domain.report.Report;
import pl.agh.knbit.domain.semester.event.EventType;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class EventsReplacer implements IReplacer {
    private static final double AVERAGE_DURATION = 1.5;

    @Override
    public String token() {
        throw new NotImplementedException();
    }

    @Override
    public String apply(String input, Report reportData) {
        long workshopsCount = reportData.events()
                .stream()
                .filter(ev -> ev.eventType() == EventType.WORKSHOP)
                .count();

        input = input.replace("${events-workshops-count}", String.valueOf(workshopsCount));
        input = input.replace("${events-workshops-total-time}", String.valueOf(workshopsCount * AVERAGE_DURATION) + " h");


        long lecturesCount = reportData.events()
                .stream()
                .filter(ev -> ev.eventType() == EventType.LECTURE)
                .count();

        input = input.replace("${events-lectures-count}", String.valueOf(lecturesCount));
        input = input.replace("${events-lectures-total-time}", String.valueOf(lecturesCount * AVERAGE_DURATION) + " h");

        return input;
    }
}
