package pl.agh.knbit.domain.report.subscribers;

import pl.agh.knbit.domain.report.Report;
import pl.agh.knbit.domain.report.ReportQuery;
import pl.agh.knbit.domain.report.ReportsEventConsumer;
import pl.agh.knbit.domain.semester.section.Section;
import pl.agh.knbit.domain.semester.section.SectionId;
import pl.agh.knbit.generated.protobuffs.SectionsBc;
import pl.agh.knbit.portadapter.kafka.serialization.protobuffs.ProtoBuffsEventDeserializer;

import java.util.Objects;
import java.util.function.Consumer;

public class MemberJoinedSectionReportsConsumer implements ReportsEventConsumer {
    @Override
    public String commitLogTopic() {
        return "MemberJoinedSectionEvent";
    }

    @Override
    public Consumer<byte[]> eventConsumer(Report report, ReportQuery query) {
        return (bytes) -> {
            SectionsBc.MemberJoinedSectionEvent event =
                    ProtoBuffsEventDeserializer.deserialize(bytes, SectionsBc.MemberJoinedSectionEvent.class);

            if (Objects.isNull(event)) {
                return;
            }

            if (query.wasEventBeforeQueryEndDate(event.getUtcDateAsEpochSeconds())) {
                report.sections()
                        .stream()
                        .filter(s -> s.sectionId().equals(SectionId.of(event.getSectionId())))
                        .findFirst()
                        .ifPresent(Section::addMember);
            }
        };
    }
}
