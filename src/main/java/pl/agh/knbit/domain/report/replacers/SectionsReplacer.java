package pl.agh.knbit.domain.report.replacers;

import pl.agh.knbit.domain.report.Report;

public class SectionsReplacer implements IReplacer {
    @Override
    public String token() {
        return "${sections}";
    }

    @Override
    public String apply(String input, Report reportData) {
        return input.replace(token(), buildSections(reportData));
    }

    private String buildSections(Report reportData) {
        StringBuilder builder = new StringBuilder();
        reportData.sections()
                .forEach(section -> {
                    builder.append("\n\n");

                    String nextSection = sectionTemplate();
                    nextSection = nextSection.replace("${section-name}", section.name());
                    nextSection = nextSection.replace("${section-description}", section.description());
                    nextSection = nextSection.replace("${section-members-count}", String.valueOf(section.memberCount()));

                    builder.append(nextSection);
                });

        return builder.toString();
    }

    private String sectionTemplate() {
        return "${section-name}\n" +
                "${section-description}\n" +
                "Liczba członków: ${section-members-count}";
    }
}
