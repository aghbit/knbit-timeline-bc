package pl.agh.knbit.domain.report.replacers;

import pl.agh.knbit.domain.report.Report;

public class MembersCountReplacer implements IReplacer {
    @Override
    public String token() {
        return "${members-count}";
    }

    @Override
    public String apply(String input, Report reportData) {
        int membersCount = countMembers(reportData);
        return input.replace(token(), String.valueOf(membersCount));
    }

    private static int countMembers(Report reportData) {
        return reportData.members().size();
    }
}
