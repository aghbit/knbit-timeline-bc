package pl.agh.knbit.domain.report.replacers;

import pl.agh.knbit.domain.report.Report;

public class MembersLeftCountReplacer implements IReplacer {
    @Override
    public String token() {
        return "${members-left-count}";
    }

    @Override
    public String apply(String input, Report reportData) {
        int membersCount = countLeftMembers(reportData);
        return input.replace(token(), String.valueOf(membersCount));
    }

    private static int countLeftMembers(Report reportData) {
        return reportData.leftMembers().size();
    }
}
