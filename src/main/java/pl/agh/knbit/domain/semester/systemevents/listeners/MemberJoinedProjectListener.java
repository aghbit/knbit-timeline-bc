package pl.agh.knbit.domain.semester.systemevents.listeners;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.agh.knbit.domain.feeds.Feed;
import pl.agh.knbit.domain.feeds.FeedRepository;
import pl.agh.knbit.domain.semester.Member;
import pl.agh.knbit.domain.semester.Semester;
import pl.agh.knbit.domain.semester.project.Project;
import pl.agh.knbit.domain.semester.project.ProjectId;
import pl.agh.knbit.domain.semester.repository.SemesterRepository;
import pl.agh.knbit.domain.semester.systemevents.SemesterFactory;
import pl.agh.knbit.domain.semester.systemevents.exception.NoSuchProjectException;
import pl.agh.knbit.generated.protobuffs.ProjectsBc;
import pl.agh.knbit.util.Replacer;

import java.util.Optional;

import static pl.agh.knbit.portadapter.kafka.serialization.protobuffs.ProtoBuffsEventDeserializer.deserialize;

@Component
@Transactional
public class MemberJoinedProjectListener extends KafkaTopicListener<ProjectsBc.MemberJoinedProjectEvent> {

    public static final String FEED_PATTERN = "${members-first-name} ${member-last-name} joined ${project-name} project!";

    @Autowired
    public MemberJoinedProjectListener(SemesterRepository repository,
                                       SemesterFactory factory,
                                       FeedRepository feedRepository) {
        super(repository, factory, feedRepository);
    }

    @Override
    public void onNextMessage(byte[] message) {
        try {
            ProjectsBc.MemberJoinedProjectEvent event = deserialize(message, ProjectsBc.MemberJoinedProjectEvent.class);
            onNextEvent(event);
        } catch (Exception e) {
            onError(e);
        }
    }

    @Override
    protected Semester updateSemester(Semester semester, ProjectsBc.MemberJoinedProjectEvent e) {
        Project project = semester.projects()
                .stream()
                .filter(p -> p.projectId().equals(ProjectId.of(e.getProjectId())))
                .findFirst()
                .orElseThrow(() -> new NoSuchProjectException(e.getProjectId()));

        project.addParticipant(Member.of(e.getMemberId()));
        return semester;
    }

    @Override
    protected Optional<Feed> buildFeed(ProjectsBc.MemberJoinedProjectEvent event) {
        String message = Replacer.replace(FEED_PATTERN)
                .with("${members-first-name}", event.getMemberFirstName())
                .with("${member-last-name}", event.getMemberLastName())
                .with("${project-name}", event.getProjectName())
                .toString();

        return Optional.of(new Feed(message, event.getUtcDateAsEpochSeconds(), topicName()));
    }

    @Override
    public String topicName() {
        return "MemberJoinedProjectEvent";
    }
}
