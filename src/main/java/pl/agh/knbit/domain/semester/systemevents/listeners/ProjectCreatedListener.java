package pl.agh.knbit.domain.semester.systemevents.listeners;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.agh.knbit.domain.feeds.Feed;
import pl.agh.knbit.domain.feeds.FeedRepository;
import pl.agh.knbit.domain.semester.Semester;
import pl.agh.knbit.domain.semester.project.Project;
import pl.agh.knbit.domain.semester.repository.SemesterRepository;
import pl.agh.knbit.domain.semester.systemevents.SemesterFactory;
import pl.agh.knbit.generated.protobuffs.ProjectsBc;
import pl.agh.knbit.util.Replacer;

import java.util.Optional;

import static org.apache.commons.lang.StringUtils.isEmpty;
import static pl.agh.knbit.portadapter.kafka.serialization.protobuffs.ProtoBuffsEventDeserializer.deserialize;

@Component
@Transactional
public class ProjectCreatedListener extends KafkaTopicListener<ProjectsBc.ProjectCreatedEvent> {
    public static final String FEED_PATTERN = "New project ${project-name} created! Join now! ${client-info}!";


    @Autowired
    public ProjectCreatedListener(SemesterRepository repository,
                                  SemesterFactory factory,
                                  FeedRepository feedRepository) {
        super(repository, factory, feedRepository);
    }


    @Override
    public void onNextMessage(byte[] message) {
        try {
            ProjectsBc.ProjectCreatedEvent event = deserialize(message, ProjectsBc.ProjectCreatedEvent.class);
            onNextEvent(event);
        } catch (Exception e) {
            onError(e);
        }
    }

    @Override
    protected Semester updateSemester(Semester semester, ProjectsBc.ProjectCreatedEvent e) {
        Project project = Project.fromEvent(e);

        semester.projects()
                .add(project);

        return semester;
    }

    @Override
    public Optional<Feed> buildFeed(ProjectsBc.ProjectCreatedEvent event) {
        String message = Replacer.replace(FEED_PATTERN)
                .with("${project-name}", event.getProjectName())
                .with("${client-info}", "Our client is " + event.getClientName(), "", () -> !isEmpty(event.getClientName()))
                .toString();

        return Optional.of(new Feed(message, event.getUtcDateAsEpochSeconds(), "EventTookPlaceEvent"));
    }


    @Override
    public String topicName() {
        return "ProjectCreatedEvent";
    }
}
