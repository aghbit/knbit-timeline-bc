package pl.agh.knbit.domain.semester;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;
import lombok.experimental.Accessors;
import pl.agh.knbit.domain.semester.event.Event;
import pl.agh.knbit.domain.semester.project.Project;
import pl.agh.knbit.domain.semester.project.ProjectId;
import pl.agh.knbit.domain.semester.section.Section;
import pl.agh.knbit.domain.semester.section.SectionId;
import pl.agh.knbit.domain.semester.systemevents.exception.NoSuchProjectException;
import pl.agh.knbit.domain.semester.systemevents.exception.NoSuchSectionException;

import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import java.util.HashSet;
import java.util.Set;

import static com.google.common.base.Preconditions.checkArgument;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.FetchType.EAGER;
import static javax.persistence.FetchType.LAZY;

/**
 * @author Lukasz Raduj 2015 <raduj.lukasz at gmail.com>
 */
@Entity
@Data
@Accessors(fluent = true, chain = true)
@Setter(value = AccessLevel.PRIVATE)
public class Semester {
    private int year;
    private Season season;
    private boolean isDraft;

    @EmbeddedId
    @JsonUnwrapped
    protected SemesterId semesterId;

    @Embedded
    protected PeriodDuration periodDuration;
    protected long commitsCount;

    @ManyToMany(fetch = LAZY, cascade = {PERSIST, MERGE})
    private Set<Member> members = new HashSet<>();

    @ManyToMany(fetch = LAZY, cascade = {PERSIST, MERGE})
    protected Set<Project> projects = new HashSet<>();

    @ManyToMany(fetch = EAGER, cascade = {PERSIST, MERGE})
    protected Set<Section> sections = new HashSet<>();

    @ManyToMany(fetch = EAGER, cascade = {PERSIST, MERGE})
    protected Set<Event> events = new HashSet<>();

    public Semester(SemesterId semesterId) {
        this.semesterId = semesterId;
        this.periodDuration = PeriodDuration.of(semesterId);
        this.commitsCount = 0;
        this.year = semesterId.getYear();
        this.season = semesterId.getSeason();
        this.isDraft = true;
    }

    public static Semester of(Semester existing, SemesterId newSemesterId) {
        Semester semester = new Semester(newSemesterId);

        semester.addMembers(existing.members());
        semester.addProjects(existing.projects());
        semester.addSections(existing.sections());

        return semester;
    }

    public void activate() {
        this.isDraft = false;
    }

    public void addMembers(Set<Member> members) {
        this.members.addAll(members);
    }

    public void addProjects(Set<Project> projects) {
        this.projects.addAll(projects);
    }

    public void addSections(Set<Section> sections) {
        this.sections.addAll(sections);
    }

    public void addEvents(Set<Event> events) {
        this.events.addAll(events);
    }

    public void increaseCommitsCount(long commitsCount) {
        checkArgument(commitsCount > this.commitsCount, "Commits count cannot be decreased");
        this.commitsCount = commitsCount;
    }

    public Section findSectionById(SectionId sectionId) {
        return sections.stream()
                .filter(section -> section.sectionId().equals(sectionId))
                .findFirst()
                .orElseThrow(() -> new NoSuchSectionException(sectionId));
    }

    public void addSection(Section section) {
        this.sections.add(section);
    }

    public Project findProjectById(ProjectId projectId) {
        return projects.stream()
                .filter(p -> p.projectId().equals(projectId))
                .findFirst()
                .orElseThrow(() -> new NoSuchProjectException(projectId.getProjectId()));
    }

    private Semester() {
    } //for JPA
}
