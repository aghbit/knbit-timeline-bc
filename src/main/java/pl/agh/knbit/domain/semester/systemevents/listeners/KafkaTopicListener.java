package pl.agh.knbit.domain.semester.systemevents.listeners;

import com.google.protobuf.MessageLite;
import lombok.extern.slf4j.Slf4j;
import pl.agh.knbit.domain.feeds.Feed;
import pl.agh.knbit.domain.feeds.FeedRepository;
import pl.agh.knbit.domain.semester.Semester;
import pl.agh.knbit.domain.semester.SemesterId;
import pl.agh.knbit.domain.semester.repository.SemesterRepository;
import pl.agh.knbit.domain.semester.systemevents.SemesterFactory;
import pl.agh.knbit.util.DateTimeUtils;

import java.time.LocalDateTime;
import java.util.Optional;

@Slf4j
public abstract class KafkaTopicListener<T extends MessageLite> {
    protected final SemesterRepository semesterRepository;
    protected final SemesterFactory semesterFactory;
    protected final FeedRepository feedRepository;

    protected KafkaTopicListener(SemesterRepository semesterRepository,
                                 SemesterFactory semesterFactory,
                                 FeedRepository feedRepository) {
        this.semesterRepository = semesterRepository;
        this.semesterFactory = semesterFactory;
        this.feedRepository = feedRepository;
    }

    public abstract void onNextMessage(byte[] message);

    protected Optional<Feed> buildFeed(T event) {
        return Optional.empty();
    }

    protected abstract Semester updateSemester(Semester semester, T event);

    protected void onNextEvent(T event) throws Exception {
        LocalDateTime eventDateTime = DateTimeUtils.getEventOccurrenceDate(event);
        SemesterId collocatedSemesterId = SemesterId.of(eventDateTime.toLocalDate());

        Semester semester = findMatchingSemester(collocatedSemesterId);
        Semester afterUpdateSemester = updateSemester(semester, event);
        buildFeed(event).ifPresent(feedRepository::save);

        semester.activate();
        semesterRepository.save(afterUpdateSemester);
        log.info("Successfully processed event of type: {}", event.getClass().getSimpleName());
    }

    protected void onError(Exception ex) {
        log.error("Cannot process kafka message {}! Reason: ", topicName(), ex);
    }

    protected Semester findMatchingSemester(SemesterId collocatedSemesterId) {
        if (semesterRepository.exists(collocatedSemesterId)) {
            return semesterRepository.findOne(collocatedSemesterId);
        } else {
            return semesterFactory.newDraftFromPreviousSemester(collocatedSemesterId);
        }
    }

    public abstract String topicName();
}
