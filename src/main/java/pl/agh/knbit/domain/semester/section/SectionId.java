package pl.agh.knbit.domain.semester.section;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import java.io.Serializable;

import static lombok.AccessLevel.PRIVATE;

/**
 * @author Lukasz Raduj 2015 <raduj.lukasz at gmail.com>
 */
@Embeddable
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor(access = PRIVATE)
@EqualsAndHashCode
@Getter
public class SectionId implements Serializable {
    private String sectionId;
}
