package pl.agh.knbit.domain.semester.systemevents.listeners;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.agh.knbit.domain.feeds.FeedRepository;
import pl.agh.knbit.domain.semester.Semester;
import pl.agh.knbit.domain.semester.SemesterId;
import pl.agh.knbit.domain.semester.repository.SemesterRepository;
import pl.agh.knbit.domain.semester.systemevents.SemesterFactory;
import pl.agh.knbit.generated.protobuffs.TimelineBc;

import static pl.agh.knbit.portadapter.kafka.serialization.protobuffs.ProtoBuffsEventDeserializer.deserialize;

@Component
@Transactional
public class TotalCommitsCountListener extends KafkaTopicListener<TimelineBc.TotalCommitsCountUpdateEvent> {

    @Autowired
    public TotalCommitsCountListener(SemesterRepository repository,
                                     SemesterFactory factory,
                                     FeedRepository feedRepository) {
        super(repository, factory, feedRepository);
    }


    @Override
    public void onNextMessage(byte[] message) {
        try {
            TimelineBc.TotalCommitsCountUpdateEvent event = deserialize(message, TimelineBc.TotalCommitsCountUpdateEvent.class);
            onNextEvent(event);
        } catch (Exception e) {
            onError(e);
        }
    }

    @Override
    protected Semester updateSemester(Semester semester, TimelineBc.TotalCommitsCountUpdateEvent event) {
        SemesterId previousSemesterId = semester.semesterId().previous();

        long previousSemesterCommitsCount = 0;
        if (semesterRepository.exists(previousSemesterId)) {
            Semester previousSemester = semesterRepository.findOne(previousSemesterId);
            previousSemesterCommitsCount = previousSemester.commitsCount();
        }

        semester.increaseCommitsCount(event.getCommitsCount() - previousSemesterCommitsCount);
        return semester;
    }

    @Override
    public String topicName() {
        return "TotalCommitsCountUpdateEvent";
    }
}
