package pl.agh.knbit.domain.semester.event;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * @author Lukasz Raduj 2015 <raduj.lukasz at gmail.com>
 */
@Embeddable
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor
@EqualsAndHashCode
@Getter
public class EventId implements Serializable {
    private String eventId;
}
