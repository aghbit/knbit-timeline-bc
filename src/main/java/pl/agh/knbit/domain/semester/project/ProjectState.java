package pl.agh.knbit.domain.semester.project;

/**
 * @author Lukasz Raduj 2015 <raduj.lukasz at gmail.com>
 */
public enum ProjectState {
    NOT_STARTED,
    IN_PROGRESS,
    FINISHED
}
