package pl.agh.knbit.domain.semester;

import java.time.Month;
import java.time.MonthDay;

/**
 * @author Lukasz Raduj 2015 <raduj.lukasz at gmail.com>
 */
public enum Season {
    WINTER(MonthDay.of(Month.OCTOBER, 1), MonthDay.of(Month.FEBRUARY, 21)),
    SUMMER(MonthDay.of(Month.FEBRUARY, 22), MonthDay.of(Month.SEPTEMBER, 30));

    private final MonthDay beginningDate;
    private final MonthDay endDate;

    Season(MonthDay beginningDate, MonthDay endDate) {
        this.beginningDate = beginningDate;
        this.endDate = endDate;
    }

    public MonthDay beginningDate() {
        return beginningDate;
    }

    public MonthDay endDate() {
        return endDate;
    }
}
