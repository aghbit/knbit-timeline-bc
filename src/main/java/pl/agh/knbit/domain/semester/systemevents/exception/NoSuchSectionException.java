package pl.agh.knbit.domain.semester.systemevents.exception;

import pl.agh.knbit.domain.semester.section.SectionId;

import static java.lang.String.format;

public class NoSuchSectionException extends RuntimeException {
    public NoSuchSectionException(SectionId sectionId) {
        super(format("Cannot find section of id: %s", sectionId));
    }
}
