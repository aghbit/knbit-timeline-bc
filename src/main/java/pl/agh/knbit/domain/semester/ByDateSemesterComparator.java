package pl.agh.knbit.domain.semester;

import java.util.Comparator;

public class ByDateSemesterComparator implements Comparator<Semester>  {
    @Override
    public int compare(Semester left, Semester right) {
        int yearDiff = Integer.compare(left.year(), right.year());
        if (yearDiff != 0) {
            return yearDiff;
        }

        if (left.season() == Season.SUMMER) {
            return 1;
        } else {
            return -1;
        }
    }
}
