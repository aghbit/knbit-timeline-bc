package pl.agh.knbit.domain.semester.systemevents.listeners;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.agh.knbit.domain.feeds.Feed;
import pl.agh.knbit.domain.feeds.FeedRepository;
import pl.agh.knbit.domain.semester.Member;
import pl.agh.knbit.domain.semester.Semester;
import pl.agh.knbit.domain.semester.repository.SemesterRepository;
import pl.agh.knbit.domain.semester.systemevents.SemesterFactory;
import pl.agh.knbit.generated.protobuffs.MembersBc;
import pl.agh.knbit.portadapter.kafka.serialization.protobuffs.ProtoBuffsEventDeserializer;
import pl.agh.knbit.util.Replacer;

import java.util.Optional;

@Component
@Transactional
public class MemberJoinedKNBITListener extends KafkaTopicListener<MembersBc.MemberJoinedKNBITEvent> {
    public static final String FEED_PATTERN = "${first-name} ${last-name} joined KN BIT. Welcome!";

    @Autowired
    public MemberJoinedKNBITListener(SemesterRepository repository,
                                     SemesterFactory factory,
                                     FeedRepository feedRepository) {
        super(repository, factory, feedRepository);
    }

    @Override
    public void onNextMessage(byte[] eventBytes) {
        try {
            MembersBc.MemberJoinedKNBITEvent event =
                    ProtoBuffsEventDeserializer.deserialize(eventBytes, MembersBc.MemberJoinedKNBITEvent.class);

            onNextEvent(event);
        } catch (Exception ex) {
            onError(ex);
        }
    }

    @Override
    public Semester updateSemester(Semester semester, MembersBc.MemberJoinedKNBITEvent message) {
        semester.members()
                .add(Member.of(message.getMemberId()));

        return semester;
    }

    @Override
    public String topicName() {
        return "MemberJoinedKNBITEvent";
    }

    @Override
    protected Optional<Feed> buildFeed(MembersBc.MemberJoinedKNBITEvent event) {
        String message = Replacer.replace(FEED_PATTERN)
                .with("${first-name}", event.getMemberFirstName())
                .with("${last-name}", event.getMemberLastName())
                .toString();

        return Optional.of(new Feed(message, event.getUtcDateAsEpochSeconds(), topicName()));
    }
}
