package pl.agh.knbit.domain.semester.systemevents.listeners;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.agh.knbit.domain.feeds.Feed;
import pl.agh.knbit.domain.feeds.FeedRepository;
import pl.agh.knbit.domain.semester.Semester;
import pl.agh.knbit.domain.semester.event.Event;
import pl.agh.knbit.domain.semester.repository.SemesterRepository;
import pl.agh.knbit.domain.semester.systemevents.SemesterFactory;
import pl.agh.knbit.generated.protobuffs.EventsBc;
import pl.agh.knbit.util.Replacer;

import java.util.Optional;

import static pl.agh.knbit.portadapter.kafka.serialization.protobuffs.ProtoBuffsEventDeserializer.deserialize;

@Component
@Transactional
public class EventTookPlaceListener extends KafkaTopicListener<EventsBc.EventTookPlaceEvent> {

    public static final String FEED_PATTERN = "${members-info} learnt cool things on ${event-name} event";


    @Autowired
    public EventTookPlaceListener(SemesterRepository repository,
                                  SemesterFactory factory,
                                  FeedRepository feedRepository) {
        super(repository, factory, feedRepository);
    }

    @Override
    public void onNextMessage(byte[] message) {
        try {
            EventsBc.EventTookPlaceEvent event = deserialize(message, EventsBc.EventTookPlaceEvent.class);
            onNextEvent(event);
        } catch (Exception e) {
            onError(e);
        }
    }

    @Override
    protected Semester updateSemester(Semester semester, EventsBc.EventTookPlaceEvent e) {
        Event event = Event.fromEvent(e);

        semester.events()
                .add(event);

        return semester;
    }

    @Override
    public String topicName() {
        return "EventTookPlaceEvent";
    }

    @Override
    protected Optional<Feed> buildFeed(EventsBc.EventTookPlaceEvent event) {
        String message = Replacer.replace(FEED_PATTERN)
                .with("${members-info}", event.getAttendesCount() + " people", "Our members", () -> event.getAttendesCount() > 0)
                .with("${event-name}", event.getEventName())
                .toString();

        return Optional.of(new Feed(message, event.getUtcDateAsEpochSeconds(), "EventTookPlaceEvent"));
    }
}
