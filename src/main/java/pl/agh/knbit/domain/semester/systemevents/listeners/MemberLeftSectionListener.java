package pl.agh.knbit.domain.semester.systemevents.listeners;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.agh.knbit.domain.feeds.Feed;
import pl.agh.knbit.domain.feeds.FeedRepository;
import pl.agh.knbit.domain.semester.Semester;
import pl.agh.knbit.domain.semester.repository.SemesterRepository;
import pl.agh.knbit.domain.semester.section.Section;
import pl.agh.knbit.domain.semester.section.SectionId;
import pl.agh.knbit.domain.semester.systemevents.SemesterFactory;
import pl.agh.knbit.generated.protobuffs.SectionsBc;
import pl.agh.knbit.util.Replacer;

import java.util.Optional;

import static pl.agh.knbit.portadapter.kafka.serialization.protobuffs.ProtoBuffsEventDeserializer.deserialize;

@Component
@Transactional
public class MemberLeftSectionListener extends KafkaTopicListener<SectionsBc.MemberLeftSectionEvent> {
    private String sectionName;
    public static final String FEED_PATTERN = "Member of id: ${member-id} left section: ${section-name}. Reason: ${reason}";

    @Autowired
    public MemberLeftSectionListener(SemesterRepository repository,
                                     SemesterFactory factory,
                                     FeedRepository feedRepository) {
        super(repository, factory, feedRepository);
    }

    @Override
    public void onNextMessage(byte[] message) {
        try {
            SectionsBc.MemberLeftSectionEvent event = deserialize(message, SectionsBc.MemberLeftSectionEvent.class);
            onNextEvent(event);
        } catch (Exception e) {
            onError(e);
        }
    }

    @Override
    protected Semester updateSemester(Semester semester, SectionsBc.MemberLeftSectionEvent e) {
        Section section = semester.findSectionById(SectionId.of(e.getSectionId()));

        this.sectionName = section.name();
        section.removeMember();

        return semester;
    }

    @Override
    protected Optional<Feed> buildFeed(SectionsBc.MemberLeftSectionEvent event) {
        String message = Replacer.replace(FEED_PATTERN)
                .with("${member-id}", event.getMemeberId())
                .with("${section-name}", sectionName, event.getSectionId())
                .with("${reason}", event.getReason(), "NOT-PROVIDED")
                .toString();

        return Optional.of(new Feed(message, event.getUtcDateAsEpochSeconds(), topicName()));
    }

    @Override
    public String topicName() {
        return "MemberLeftSectionEvent";
    }
}
