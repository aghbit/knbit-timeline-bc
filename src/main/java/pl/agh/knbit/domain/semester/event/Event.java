package pl.agh.knbit.domain.semester.event;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.experimental.Accessors;
import lombok.experimental.Builder;
import pl.agh.knbit.domain.semester.Member;
import pl.agh.knbit.domain.semester.section.SectionId;
import pl.agh.knbit.generated.protobuffs.EventsBc;

import javax.persistence.*;
import java.util.Set;

import static java.util.stream.Collectors.toSet;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.FetchType.EAGER;

/**
 * @author Lukasz Raduj 2015 <raduj.lukasz at gmail.com>
 */
@Entity
@Builder
@Getter
@Accessors(fluent = true, chain = true)
@AllArgsConstructor
@EqualsAndHashCode(of = "eventId")
public class Event {

    @EmbeddedId
    @JsonUnwrapped
    private EventId eventId;
    private String title;
    @Column(length = 2000)
    private String description;
    private EventType eventType;
    private String pictureUrl;

    @Embedded
    @JsonUnwrapped
    private SectionId sectionId;

    @ManyToMany(fetch = EAGER, cascade = {PERSIST, MERGE})
    private Set<Member> speakers;

    public static Event fromEvent(EventsBc.EventTookPlaceEvent e) {
        Set<Member> speakersIds = e.getSpeakersList()
                .stream()
                .map(speaker -> Member.of(speaker.getAccountId()))
                .collect(toSet());

        return new Event(EventId.of(e.getEventId()),
                e.getEventName(),
                e.getEventDescription(),
                EventType.valueOf(e.getEventType().name()),
                e.getEventPictureUrl(),
                SectionId.of(e.getSectionId()),
                speakersIds);
    }

    private Event() {
    } //for JPA
}
