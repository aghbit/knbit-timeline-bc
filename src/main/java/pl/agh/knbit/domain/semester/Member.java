package pl.agh.knbit.domain.semester;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.Entity;
import javax.persistence.Id;

import static lombok.AccessLevel.PRIVATE;

/**
 * @author Lukasz Raduj 2015 <raduj.lukasz at gmail.com>
 */
@Entity
@Data
@AllArgsConstructor
@Accessors(fluent = true)
@Setter(value = PRIVATE)
public class Member {

    @Id
    private String memberId;

    public static Member of(String memberId) {
        return new Member(memberId);
    }

    private Member() {
    } //for JPA
}
