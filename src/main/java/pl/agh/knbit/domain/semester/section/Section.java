package pl.agh.knbit.domain.semester.section;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.experimental.Builder;
import pl.agh.knbit.generated.protobuffs.SectionsBc;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

/**
 * @author Lukasz Raduj 2015 <raduj.lukasz at gmail.com>
 */
@Entity
@Builder
@Data
@Accessors(fluent = true, chain = true)
@AllArgsConstructor
@EqualsAndHashCode(doNotUseGetters = true, of = "sectionId")
public class Section {

    @JsonUnwrapped
    @EmbeddedId
    private SectionId sectionId;
    private String name;
    @Column(length = 2000)
    private String description;
    private int memberCount;
    private String logoUrl;

    public void addMember() {
        memberCount++;
    }

    public void removeMember() {
        memberCount--;
    }

    public static Section fromEvent(SectionsBc.SectionCreatedEvent event) {
        return Section.builder()
                .sectionId(SectionId.of(event.getSectionId()))
                .name(event.getSectionName())
                .description(event.getSectionDescription())
                .memberCount(0)
                .logoUrl(event.getLogoUrl())
                .build();
    }

    private Section() {
    } // why JPA why
}
