package pl.agh.knbit.domain.semester.systemevents.listeners;

import com.google.common.collect.ImmutableMap;
import kafka.consumer.Consumer;
import kafka.consumer.ConsumerConfig;
import kafka.consumer.ConsumerIterator;
import kafka.consumer.KafkaStream;
import kafka.javaapi.consumer.ConsumerConnector;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Slf4j
public class KafkaTopicConsumer {
    private final static ExecutorService executorService = Executors.newCachedThreadPool();
    private final String topicName;
    private final KafkaTopicListener kafkaTopicListener;
    private final Properties consumerProperties;

    public KafkaTopicConsumer(Properties consumerProperties, KafkaTopicListener kafkaTopicListener) {
        this.consumerProperties = consumerProperties;
        this.topicName = kafkaTopicListener.topicName();
        this.kafkaTopicListener = kafkaTopicListener;
    }

    public void startConsuming() {
        executorService.submit(() -> {
            try {
                ImmutableMap<String, Integer> topicCountMapBuilder = ImmutableMap.of(topicName, 1);

                consumerProperties.put("group.id", topicName + "Consumer");

                ConsumerConnector consumerConnector = Consumer.createJavaConsumerConnector(new ConsumerConfig(consumerProperties));

                Map<String, List<KafkaStream<byte[], byte[]>>> messageStreams = consumerConnector.createMessageStreams(topicCountMapBuilder);

                List<KafkaStream<byte[], byte[]>> streams = messageStreams.get(topicName);
                ConsumerIterator<byte[], byte[]> it = streams.get(0)
                        .iterator();

                while (it.hasNext()) {
                    synchronized (this.getClass()) {
                        log.info("[KAFKA] Received new event on topic {} !", topicName);

                        kafkaTopicListener.onNextMessage(it.next().message());
                    }
                }
            } catch (Exception ex) {
                kafkaTopicListener.onError(ex);
            }
        });
    }
}
