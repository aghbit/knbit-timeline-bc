package pl.agh.knbit.domain.semester;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.agh.knbit.domain.semester.repository.SemesterRepository;
import pl.agh.knbit.portadapter.httprest.adapters.springmvc.dto.PeriodIdDTO;

import java.util.HashSet;
import java.util.Set;

import static java.util.stream.Collectors.toSet;

/**
 * @author Lukasz Raduj 2015 raduj.lukasz at gmail.com
 */
@Component
public class PeriodService {
    public final static SemesterId SYMBOLIC_BEGINNING_OF_BIT = SemesterId.of("2000-WINTER");
    private final SemesterRepository semesterRepository;

    @Autowired
    public PeriodService(SemesterRepository semesterRepository) {
        this.semesterRepository = semesterRepository;
    }

    public Set<PeriodIdDTO> findAllExistingPeriodIds() {
        SemesterId current = SYMBOLIC_BEGINNING_OF_BIT;
        Set<SemesterId> ids = new HashSet<>();
        SemesterId currentSemester = SemesterId.ofCurrentSemester();
        while(!current.equals(currentSemester)) {
            current = current.next();
            ids.add(current);
        }

        return ids.stream()
                .filter(semesterRepository::exists)
                .map(PeriodIdDTO::new)
                .collect(toSet());
    }
}
