package pl.agh.knbit.domain.semester.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.agh.knbit.domain.semester.Semester;
import pl.agh.knbit.domain.semester.SemesterId;

@Component
public class TransactionalSemesterRepository {

    private final SemesterRepository semesterRepository;

    @Autowired
    public TransactionalSemesterRepository(SemesterRepository semesterRepository) {
        this.semesterRepository = semesterRepository;
    }

    @Transactional(readOnly = true)
    public Semester findOneFullyLoaded(SemesterId id) {
        Semester semester = semesterRepository.findOne(id);
        semester.projects().size();

        semester.projects().forEach(p -> p.participants().size());
        semester.members().size();
        semester.events().size();
        semester.events().forEach(e -> e.speakers().size());
        semester.sections().size();

        return semester;
    }
}
