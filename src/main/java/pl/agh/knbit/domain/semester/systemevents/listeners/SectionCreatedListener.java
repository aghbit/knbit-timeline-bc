package pl.agh.knbit.domain.semester.systemevents.listeners;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.agh.knbit.domain.feeds.Feed;
import pl.agh.knbit.domain.feeds.FeedRepository;
import pl.agh.knbit.domain.semester.Semester;
import pl.agh.knbit.domain.semester.repository.SemesterRepository;
import pl.agh.knbit.domain.semester.section.Section;
import pl.agh.knbit.domain.semester.systemevents.SemesterFactory;
import pl.agh.knbit.generated.protobuffs.SectionsBc;
import pl.agh.knbit.util.Replacer;

import java.util.Optional;

import static pl.agh.knbit.portadapter.kafka.serialization.protobuffs.ProtoBuffsEventDeserializer.deserialize;

@Component
@Transactional
public class SectionCreatedListener extends KafkaTopicListener<SectionsBc.SectionCreatedEvent> {
    public static final String FEED_PATTERN = "New section created! Now you can join brand new ${section-name} section";

    @Autowired
    public SectionCreatedListener(SemesterRepository repository,
                                  SemesterFactory factory,
                                  FeedRepository feedRepository) {
        super(repository, factory, feedRepository);
    }

    @Override
    public void onNextMessage(byte[] message) {
        try {
            SectionsBc.SectionCreatedEvent event = deserialize(message, SectionsBc.SectionCreatedEvent.class);
            onNextEvent(event);
        } catch (Exception e) {
            onError(e);
        }
    }

    @Override
    protected Semester updateSemester(Semester semester, SectionsBc.SectionCreatedEvent event) {
        Section section = Section.fromEvent(event);

        semester.addSection(section);
        return semester;
    }

    @Override
    public Optional<Feed> buildFeed(SectionsBc.SectionCreatedEvent event) {
        String message = Replacer.replace(FEED_PATTERN)
                .with("${section-name}", event.getSectionName())
                .toString();

        return Optional.of(new Feed(message, event.getUtcDateAsEpochSeconds(), topicName()));
    }

    @Override
    public String topicName() {
        return "SectionCreatedEvent";
    }
}
