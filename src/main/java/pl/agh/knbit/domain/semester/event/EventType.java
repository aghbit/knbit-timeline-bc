package pl.agh.knbit.domain.semester.event;

/**
 * @author Lukasz Raduj 2015 <raduj.lukasz at gmail.com>
 */
public enum EventType {
    LECTURE,
    WORKSHOP,
    STUDY_GROUP
}
