package pl.agh.knbit.domain.semester.systemevents.listeners;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.agh.knbit.domain.feeds.Feed;
import pl.agh.knbit.domain.feeds.FeedRepository;
import pl.agh.knbit.domain.semester.Semester;
import pl.agh.knbit.domain.semester.repository.SemesterRepository;
import pl.agh.knbit.domain.semester.section.Section;
import pl.agh.knbit.domain.semester.section.SectionId;
import pl.agh.knbit.domain.semester.systemevents.SemesterFactory;
import pl.agh.knbit.generated.protobuffs.SectionsBc;
import pl.agh.knbit.util.Replacer;

import java.util.Optional;

import static pl.agh.knbit.portadapter.kafka.serialization.protobuffs.ProtoBuffsEventDeserializer.deserialize;

@Component
@Transactional
public class MemberJoinedSectionListener extends KafkaTopicListener<SectionsBc.MemberJoinedSectionEvent> {
    public static final String FEED_PATTERN = "Member ${first-name} ${last-name} joined ${section-name} section!";

    @Autowired
    public MemberJoinedSectionListener(SemesterRepository repository,
                                       SemesterFactory factory,
                                       FeedRepository feedRepository) {
        super(repository, factory, feedRepository);
    }


    @Override
    public void onNextMessage(byte[] message) {
        try {
            SectionsBc.MemberJoinedSectionEvent event = deserialize(message, SectionsBc.MemberJoinedSectionEvent.class);
            onNextEvent(event);
        } catch (Exception e) {
            onError(e);
        }
    }

    @Override
    protected Semester updateSemester(Semester semester, SectionsBc.MemberJoinedSectionEvent e) {
        Section section = semester.findSectionById(SectionId.of(e.getSectionId()));

        section.addMember();

        return semester;
    }


    @Override
    public Optional<Feed> buildFeed(SectionsBc.MemberJoinedSectionEvent event) {
        String message = Replacer.replace(FEED_PATTERN)
                .with("${first-name}", event.getMemberFirstName())
                .with("${last-name}", event.getMemberLastName())
                .with("${section-name}", event.getSectionName())
                .toString();

        return Optional.of(new Feed(message, event.getUtcDateAsEpochSeconds(), topicName()));
    }

    @Override
    public String topicName() {
        return "MemberJoinedSectionEvent";
    }
}
