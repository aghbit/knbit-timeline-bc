package pl.agh.knbit.domain.semester.systemevents.listeners;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Properties;

@Component
public class KafkaListenersStartupBean {
    private final Properties properties;
    private final MemberJoinedKNBITListener memberJoinedKNBITListener;
    private final EventTookPlaceListener eventTookPlaceListener;
    private final SectionCreatedListener sectionCreatedListener;
    private final MemberJoinedSectionListener memberJoinedSectionListener;
    private final MemberLeftSectionListener memberLeftSectionListener;

    private final ProjectCreatedListener projectCreatedListener;
    private final MemberJoinedProjectListener memberJoinedProjectListener;

    private final MemberLeftKNBITListener memberLeftKNBITListener;

    private final TotalCommitsCountListener totalCommitsCountListener;

    @Autowired
    public KafkaListenersStartupBean(@Qualifier("kafkaConsumerProperties") Properties properties,
                                     MemberJoinedKNBITListener memberJoinedKNBITListener,
                                     EventTookPlaceListener eventTookPlaceListener,
                                     MemberJoinedProjectListener memberJoinedProjectListener,
                                     SectionCreatedListener sectionCreatedListener,
                                     MemberJoinedSectionListener memberJoinedSectionListener,
                                     MemberLeftSectionListener memberLeftSectionListener,
                                     ProjectCreatedListener projectCreatedListener,
                                     MemberLeftKNBITListener memberLeftKNBITListener,
                                     TotalCommitsCountListener totalCommitsCountListener) {
        this.memberJoinedKNBITListener = memberJoinedKNBITListener;
        this.eventTookPlaceListener = eventTookPlaceListener;
        this.memberJoinedProjectListener = memberJoinedProjectListener;
        this.sectionCreatedListener = sectionCreatedListener;
        this.memberJoinedSectionListener = memberJoinedSectionListener;
        this.memberLeftSectionListener = memberLeftSectionListener;
        this.properties = properties;
        this.projectCreatedListener = projectCreatedListener;
        this.memberLeftKNBITListener = memberLeftKNBITListener;
        this.totalCommitsCountListener = totalCommitsCountListener;
    }

    @PostConstruct
    public void onStartup() {
        //members-bc
        new KafkaTopicConsumer(properties, memberJoinedKNBITListener).startConsuming();
        new KafkaTopicConsumer(properties, memberLeftKNBITListener).startConsuming();

        //events-bc
        new KafkaTopicConsumer(properties, eventTookPlaceListener).startConsuming();

        //projects-bc
        new KafkaTopicConsumer(properties, projectCreatedListener).startConsuming();
        new KafkaTopicConsumer(properties, memberJoinedProjectListener).startConsuming();

        //sections-bc
        new KafkaTopicConsumer(properties, sectionCreatedListener).startConsuming();
        new KafkaTopicConsumer(properties, memberJoinedSectionListener).startConsuming();
        new KafkaTopicConsumer(properties, memberLeftSectionListener).startConsuming();

        //timeline-bc
        new KafkaTopicConsumer(properties, totalCommitsCountListener).startConsuming();
    }
}
