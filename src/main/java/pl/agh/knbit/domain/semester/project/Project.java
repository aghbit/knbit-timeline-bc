package pl.agh.knbit.domain.semester.project;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.experimental.Builder;
import pl.agh.knbit.domain.semester.Member;
import pl.agh.knbit.domain.semester.section.SectionId;
import pl.agh.knbit.generated.protobuffs.ProjectsBc;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.FetchType.EAGER;
import static javax.persistence.FetchType.LAZY;

/**
 * @author Lukasz Raduj 2015 <raduj.lukasz at gmail.com>
 */
@Entity
@Builder
@Data
@Accessors(fluent = true, chain = true)
@AllArgsConstructor
@EqualsAndHashCode(of = "projectId")
public class Project {

    @EmbeddedId
    @JsonUnwrapped
    private ProjectId projectId;

    @Embedded
    @JsonUnwrapped
    private SectionId sectionId;
    private String name;
    @Column(length = 2000)
    private String description;
    private ProjectState projectState = ProjectState.NOT_STARTED;

    @ManyToMany(fetch = LAZY, cascade = {PERSIST, MERGE})
    private Set<Member> participants = new HashSet<>();

    private String logoUrl;

    public Project addParticipant(Member member) {
        participants.add(member);

        return this;
    }

    public static Project fromEvent(ProjectsBc.ProjectCreatedEvent event) {
        return Project.builder()
                .projectId(ProjectId.of(event.getProjectId()))
                .sectionId(SectionId.of(event.getSectionId()))
                .name(event.getProjectName())
                .description(event.getProjectDescription())
                .projectState(ProjectState.NOT_STARTED)
                .participants(new HashSet<>())
                .logoUrl(event.getLogoUrl())
                .build();
    }

    private Project() {
    } //for JPA
}
