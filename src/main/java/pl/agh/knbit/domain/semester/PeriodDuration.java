package pl.agh.knbit.domain.semester;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.Embeddable;
import java.time.LocalDate;

/**
 * @author Lukasz Raduj 2015 <raduj.lukasz at gmail.com>
 */
@Embeddable
@Data
@AllArgsConstructor(staticName = "of")
public class PeriodDuration {
    private LocalDate startDate;
    private LocalDate endDate;

    public static PeriodDuration of(SemesterId semesterId) {
        int year = semesterId.getYear();
        if (semesterId.getSeason() == Season.SUMMER) {
            return new PeriodDuration(Season.SUMMER.beginningDate().atYear(year), Season.SUMMER.endDate().atYear(year));
        } else {
            return new PeriodDuration(Season.WINTER.beginningDate().atYear(year), Season.WINTER.endDate().atYear(year));
        }
    }

    private PeriodDuration() {
    } //why JPA why
}
