package pl.agh.knbit.domain.semester.systemevents;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.agh.knbit.domain.semester.SemesterId;
import pl.agh.knbit.domain.semester.PeriodService;
import pl.agh.knbit.domain.semester.Semester;
import pl.agh.knbit.domain.semester.repository.SemesterRepository;

/**
 * @author Lukasz Raduj 2015 raduj.lukasz at gmail.com
 */
@Component
public class SemesterFactory {
    private final SemesterRepository semesterRepository;

    @Autowired
    public SemesterFactory(SemesterRepository semesterRepository) {
        this.semesterRepository = semesterRepository;
    }

    @Transactional
    public Semester newDraftFromPreviousSemester(SemesterId collocatedSemesterId) {
        SemesterId beginningOfBit = PeriodService.SYMBOLIC_BEGINNING_OF_BIT;

        SemesterId current = collocatedSemesterId;
        while(!current.equals(beginningOfBit)) {
            if (semesterRepository.exists(current)) {
                break;
            }
            current = current.previous();
        }

        if (semesterRepository.exists(current)) {
            return Semester.of(semesterRepository.findOne(current), collocatedSemesterId);
        } else {
            return new Semester(collocatedSemesterId);
        }
    }
}
