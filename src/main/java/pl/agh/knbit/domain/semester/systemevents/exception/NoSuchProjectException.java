package pl.agh.knbit.domain.semester.systemevents.exception;

public class NoSuchProjectException extends RuntimeException {

    public NoSuchProjectException(String projectId) {
        super("Project with id: " + projectId + " does not exists!");
    }
}
