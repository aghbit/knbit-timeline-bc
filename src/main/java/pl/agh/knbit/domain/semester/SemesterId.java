package pl.agh.knbit.domain.semester;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Embeddable;
import javax.persistence.Transient;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.MonthDay;
import java.util.regex.Pattern;

import static com.google.common.base.Preconditions.checkArgument;
import static java.lang.String.format;

/**
 * @author Lukasz Raduj 2015 <raduj.lukasz at gmail.com>
 */
@Embeddable
@Getter
@ToString
@EqualsAndHashCode
public class SemesterId implements Serializable {
    private final static Pattern ID_PATTERN = Pattern.compile("^\\d{4}\\-(WINTER|SUMMER)$");
    @Transient
    @JsonIgnore
    private int year;
    @Transient
    @JsonIgnore
    private Season season;

    private String semesterId;

    private SemesterId(String stringId) {
        checkArgument(ID_PATTERN.matcher(stringId)
                .matches(), format("Semester Id: %s of illegal format", stringId));
        this.semesterId = stringId;

        String[] split = semesterId.split("\\-");
        this.season = Season.valueOf(split[1]);
        this.year = Integer.valueOf(split[0]);
    }

    public static SemesterId of(String stringId) {
        return new SemesterId(stringId.toUpperCase());
    }

    public static SemesterId of(LocalDate date) {
        int year = parseYear(date);
        Season season = parseSeason(date);
        return SemesterId.of(format("%d-%s", year, season));
    }

    public SemesterId next() {
        if (Season.WINTER == season) {
            return SemesterId.of(format("%d-SUMMER", year));
        } else {
            return SemesterId.of(format("%d-WINTER", year + 1));
        }
    }

    public static SemesterId ofCurrentSemester() {
        return SemesterId.of(LocalDate.now());
    }

    private static int parseYear(LocalDate date) {
        MonthDay currentMonthDay = MonthDay.of(date.getMonth(), date.getDayOfMonth());
        if (isSummerSemester(currentMonthDay)) {
            //academic year starts with winter semester
            return date.getYear() - 1;
        } else {
            if (currentMonthDay.isAfter(Season.SUMMER.beginningDate())) {
                return date.getYear();
            } else {
                return date.getYear() - 1;
            }
        }
    }

    private static boolean isSummerSemester(MonthDay currentMonthDay) {
        return currentMonthDay.isAfter(Season.SUMMER.beginningDate()) && currentMonthDay.isBefore(Season.SUMMER.endDate());
    }

    private static Season parseSeason(LocalDate date) {
        MonthDay monthDayNow = MonthDay.of(date.getMonth(), date.getDayOfMonth());

        if (isSummerSemester(monthDayNow)) {
            return Season.SUMMER;
        } else {
            return Season.WINTER;
        }
    }

    public SemesterId previous() {
        if (Season.WINTER == season) {
            return SemesterId.of(format("%d-SUMMER", year - 1));
        } else {
            return SemesterId.of(format("%d-WINTER", year));
        }
    }

    private SemesterId() {
    } //why JPA why
}
