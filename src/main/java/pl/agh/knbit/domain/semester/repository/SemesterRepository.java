package pl.agh.knbit.domain.semester.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.agh.knbit.domain.semester.Semester;
import pl.agh.knbit.domain.semester.SemesterId;

/**
 * @author Lukasz Raduj 2015 <raduj.lukasz at gmail.com>
 */
public interface SemesterRepository extends JpaRepository<Semester, SemesterId> {
}
