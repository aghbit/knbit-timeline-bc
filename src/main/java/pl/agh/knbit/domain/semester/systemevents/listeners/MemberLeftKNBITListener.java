package pl.agh.knbit.domain.semester.systemevents.listeners;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.agh.knbit.domain.feeds.Feed;
import pl.agh.knbit.domain.feeds.FeedRepository;
import pl.agh.knbit.domain.semester.Member;
import pl.agh.knbit.domain.semester.Semester;
import pl.agh.knbit.domain.semester.repository.SemesterRepository;
import pl.agh.knbit.domain.semester.systemevents.SemesterFactory;
import pl.agh.knbit.generated.protobuffs.MembersBc;
import pl.agh.knbit.util.Replacer;

import java.util.Optional;

import static pl.agh.knbit.portadapter.kafka.serialization.protobuffs.ProtoBuffsEventDeserializer.deserialize;

@Component
@Transactional
public class MemberLeftKNBITListener extends KafkaTopicListener<MembersBc.MemberLeftKNBITEvent> {

    public static final String FEED_PATTERN = "Member ${first-name} ${last-name} left KN BIT. Reason ${reason}";

    @Autowired
    public MemberLeftKNBITListener(SemesterRepository repository,
                                   SemesterFactory factory,
                                   FeedRepository feedRepository) {
        super(repository, factory, feedRepository);
    }

    @Override
    public void onNextMessage(byte[] message) {
        try {
            MembersBc.MemberLeftKNBITEvent event = deserialize(message, MembersBc.MemberLeftKNBITEvent.class);
            onNextEvent(event);
        } catch (Exception e) {
            onError(e);
        }
    }

    @Override
    protected Semester updateSemester(Semester semester, MembersBc.MemberLeftKNBITEvent event) {
        semester.members()
                .removeIf(id -> id.equals(Member.of(event.getMemberId())));

        return semester;
    }

    @Override
    public Optional<Feed> buildFeed(MembersBc.MemberLeftKNBITEvent event) {

        String mes = Replacer.replace(FEED_PATTERN)
                .with("${first-name}", event.getMemberFirstName(), "-")
                .with("${last-name}", event.getMemberLastName(), "-")
                .with("${reason}", event.getReason(), "NOT-PROVIDED")
                .toString();

        return Optional.of(new Feed(mes, event.getUtcDateAsEpochSeconds(), "MemberLeftKNBITEvent"));
    }

    @Override
    public String topicName() {
        return "MemberLeftKNBITEvent";
    }
}
