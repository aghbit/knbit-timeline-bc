package pl.agh.knbit.portadapter.httprest.aabc.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import pl.agh.knbit.portadapter.httprest.aabc.AuthResponseErrorHandler;

/**
 * @author Lukasz Raduj 2015 raduj.lukasz at gmail.com
 */
@Configuration
public class AABCConfig {

    @Bean
    public RestTemplate aaBcRestTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setErrorHandler(new AuthResponseErrorHandler());

        return restTemplate;
    }
}
