package pl.agh.knbit.portadapter.httprest.aabc;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.IOException;

/**
 * @author Lukasz Raduj 2015 raduj.lukasz at gmail.com
 */
@Slf4j
public class AuthResponseErrorHandler implements ResponseErrorHandler {

    @Override
    public boolean hasError(ClientHttpResponse response) throws IOException {
        return response.getStatusCode().is4xxClientError() || response.getStatusCode().is5xxServerError();
    }

    @Override
    public void handleError(ClientHttpResponse response) throws IOException {
        log.info("Error querying outside BC! Response: {} {}", response.getStatusCode(), response.getStatusText());
    }
}
