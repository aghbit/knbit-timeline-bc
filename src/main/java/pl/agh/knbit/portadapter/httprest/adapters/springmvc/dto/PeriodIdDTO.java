package pl.agh.knbit.portadapter.httprest.adapters.springmvc.dto;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import pl.agh.knbit.domain.semester.SemesterId;
import pl.agh.knbit.domain.semester.Season;

/**
 * @author Lukasz Raduj 2015 raduj.lukasz at gmail.com
 */
public class PeriodIdDTO { //todo learn more about aggregate root
    @JsonUnwrapped
    private SemesterId semesterId;
    private int year;
    private Season season;

    public PeriodIdDTO(SemesterId semesterId) {
        this.semesterId = semesterId;
        String[] split = semesterId.getSemesterId().split("-");
        this.year = Integer.valueOf(split[0]);
        this.season = Season.valueOf(split[1]);
    }
}
