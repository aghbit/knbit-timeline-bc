package pl.agh.knbit.portadapter.httprest.adapters.springmvc;

import com.google.common.base.Stopwatch;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pl.agh.knbit.domain.semester.ByDateSemesterComparator;
import pl.agh.knbit.domain.semester.Semester;
import pl.agh.knbit.domain.semester.SemesterId;
import pl.agh.knbit.domain.semester.repository.SemesterRepository;
import pl.agh.knbit.portadapter.httprest.aabc.Authorized;
import pl.agh.knbit.portadapter.httprest.aabc.Permission;
import pl.agh.knbit.portadapter.httprest.adapters.springmvc.dto.PeriodIdDTO;
import pl.agh.knbit.portadapter.httprest.jsonresponse.JsonResponse;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static java.util.Collections.reverseOrder;
import static java.util.Objects.isNull;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * @author Lukasz Raduj 2015 <raduj.lukasz at gmail.com>
 */
@Slf4j
@RestController
@RequestMapping(value = "/api/v3",
        produces = APPLICATION_JSON_VALUE)
public class SemesterController {

    private final SemesterRepository semesterRepository;

    @Autowired
    public SemesterController(SemesterRepository semesterRepository) {
        this.semesterRepository = semesterRepository;
    }

    @Authorized(permission = Permission.READ_DRAFT_SEMESTER)
    @RequestMapping(value = "/periods/{periodId}", method = GET)
    public JsonResponse<Semester> getPeriod(@PathVariable String periodId,
                                            Principal principal) {

        log.info("Starting request....");
        Stopwatch started = Stopwatch.createStarted();
        Semester semester = semesterRepository.findOne(SemesterId.of(periodId));

        if (isNull(semester)) {
            throw new SemesterNotExistsException(periodId);
        }

        if (principal.hasRequiredPermissions()) {
            log.info("End! Time: {}", started.elapsed(TimeUnit.MILLISECONDS));
            return JsonResponse.ofSuccess(semester);
        }

        if (semester.isDraft()) {
            throw new SemesterNotExistsException(periodId);
        } else {
            log.info("End! Time: {}", started.elapsed(TimeUnit.MILLISECONDS));
            return JsonResponse.ofSuccess(semester);
        }
    }

    @ExceptionHandler(SemesterNotExistsException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public JsonResponse<String> handleSemesterNotFound(SemesterNotExistsException ex) {
        return JsonResponse.ofException(ex);
    }

    @Authorized(permission = Permission.READ_DRAFT_SEMESTER)
    @RequestMapping(value = "/periods", method = GET)
    public JsonResponse<Iterable<PeriodIdDTO>> getAllSemesters(Principal principal) {

        //possible performance issue - if true add paging/lazy load/else
        List<Semester> semesters = semesterRepository.findAll();
        if (principal.hasRequiredPermissions()) {
            List<PeriodIdDTO> semestersDTO = semesters.stream()
                    .map(sem -> new PeriodIdDTO(sem.semesterId()))
                    .collect(Collectors.toList());
            return JsonResponse.ofSuccess(semestersDTO);
        } else {
            List<PeriodIdDTO> semestersDTO = semesters.stream()
                    .filter(sem -> !sem.isDraft())
                    .map(sem -> new PeriodIdDTO(sem.semesterId()))
                    .collect(Collectors.toList());
            return JsonResponse.ofSuccess(semestersDTO);
        }
    }

    @RequestMapping(value = "/periods/last", method = GET)
    public JsonResponse<Semester> findLastCompleteSemester() {

        Semester last = semesterRepository.findAll()
                .stream()
                .filter(s -> !s.isDraft())
                .sorted(reverseOrder(new ByDateSemesterComparator()))
                .findFirst()
                .orElseThrow(() -> new SemesterNotExistsException("last"));

        return JsonResponse.ofSuccess(last);
    }
}
