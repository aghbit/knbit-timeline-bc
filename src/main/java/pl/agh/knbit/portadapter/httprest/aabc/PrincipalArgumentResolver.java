package pl.agh.knbit.portadapter.httprest.aabc;

import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;
import pl.agh.knbit.portadapter.httprest.adapters.springmvc.Principal;

import java.util.Optional;

/**
 * @author Lukasz Raduj 2015 raduj.lukasz at gmail.com
 */
public class PrincipalArgumentResolver implements HandlerMethodArgumentResolver {

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return Optional.ofNullable(parameter.getMethodAnnotation(Authorized.class)).isPresent()
                && parameter.getParameterType() == Principal.class;
    }

    @Override
    public Object resolveArgument(MethodParameter parameter,
                                  ModelAndViewContainer mavContainer,
                                  NativeWebRequest webRequest,
                                  WebDataBinderFactory binderFactory) throws Exception {
        return webRequest.getAttribute(Principal.ARGUMENT_NAME, RequestAttributes.SCOPE_REQUEST);
    }
}
