package pl.agh.knbit.portadapter.httprest.aabc;

import org.springframework.http.HttpEntity;
import org.springframework.util.LinkedMultiValueMap;

import javax.json.Json;
import javax.json.JsonObjectBuilder;

/**
 * @author Lukasz Raduj 2015 raduj.lukasz at gmail.com
 */
public class HttpEntityBuilder {
    private final LinkedMultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
    private final JsonObjectBuilder jsonObjectBuilder = Json.createObjectBuilder();

    public static HttpEntityBuilder newBuilder() {
        HttpEntityBuilder builder = new HttpEntityBuilder();
        builder.withHeader("Content-Type", "application/json");
        return builder;
    }

    public HttpEntityBuilder withHeader(String headerName, String headerValue) {
        headers.add(headerName, headerValue);

        return this;
    }

    public HttpEntityBuilder withBodyField(String fieldName, String fieldValue) {
        jsonObjectBuilder.add(fieldName, fieldValue);

        return this;
    }

    public HttpEntity<String> build() {
        return new HttpEntity<>(jsonObjectBuilder.build().toString(), headers);
    }
}
