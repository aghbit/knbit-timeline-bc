package pl.agh.knbit.portadapter.httprest.bitbucket;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

/**
 * @author Lukasz Raduj 2015 raduj.lukasz at gmail.com
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class UserProfileDTO {
    List<RepositoryDTO> repositories;
}
