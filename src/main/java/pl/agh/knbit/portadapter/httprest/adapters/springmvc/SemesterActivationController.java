package pl.agh.knbit.portadapter.httprest.adapters.springmvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.agh.knbit.domain.semester.SemesterId;
import pl.agh.knbit.domain.semester.Semester;
import pl.agh.knbit.domain.semester.repository.SemesterRepository;
import pl.agh.knbit.portadapter.httprest.jsonresponse.JsonResponse;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * @author Lukasz Raduj 2015 raduj.lukasz at gmail.com
 */
@RestController
@RequestMapping(value = "/api/v3",
        produces = APPLICATION_JSON_VALUE)
public class SemesterActivationController {

    private final SemesterRepository semesterRepository;

    @Autowired
    public SemesterActivationController(SemesterRepository semesterRepository) {
        this.semesterRepository = semesterRepository;
    }

    @RequestMapping(value = "/activate/{periodId}", method = POST)
    public JsonResponse<String> activateDraft(@PathVariable String periodId) {
        Semester semester = semesterRepository.findOne(SemesterId.of(periodId));

        semester.activate();

        semesterRepository.save(semester);
        return JsonResponse.ofSuccess();
    }
}
