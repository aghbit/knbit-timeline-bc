package pl.agh.knbit.portadapter.httprest.aabc;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 * @author Lukasz Raduj 2015 raduj.lukasz at gmail.com
 */
@Component
@Slf4j
public class AuthBcClient {
    @Value("${endpoints.aa-bc.authenticate}")
    private String authBcEndpointAuthenticate;

    @Value("${endpoints.aa-bc.authorize}")
    private String authBcEndpointAuthorize;

    @Value("${endpoints.aa-bc.authorize.header.name}")
    private String authorizeHeaderName;

    private final RestTemplate restTemplate;

    @Autowired
    public AuthBcClient(@Qualifier("aaBcRestTemplate") RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public boolean validateToken(String authToken) {
        try {
            log.info("Validating token {} with authentication/authorization BC", authToken);
            HttpEntity<String> request = HttpEntityBuilder.newBuilder()
                    .withBodyField("token", authToken)
                    .build();

            ResponseEntity<Object> response = restTemplate.postForEntity(authBcEndpointAuthenticate, request, null);
            return response.getStatusCode() == HttpStatus.OK;

        } catch (Exception ex) {
            log.error("Cannot validate token", ex);
            return false;
        }
    }

    public boolean validatePermission(@NonNull String authToken, @NonNull Permission neededPermission) {
        try {

            log.info("Validating permission {} for user with token: {}", neededPermission, authToken);

            HttpEntity<String> request = HttpEntityBuilder.newBuilder()
                    .withBodyField("permission", neededPermission.value())
                    .withHeader(authorizeHeaderName, authToken)
                    .build();

            ResponseEntity<Object> response = restTemplate.postForEntity(authBcEndpointAuthorize, request, null);
            return response.getStatusCode() == HttpStatus.OK;
        } catch (Exception ex) {
            log.error("Cannot validate permission" + neededPermission, ex);
            return false;
        }
    }
}
