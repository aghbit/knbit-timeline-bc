package pl.agh.knbit.portadapter.httprest.adapters.springmvc;

/**
 * @author Lukasz Raduj 2015 raduj.lukasz at gmail.com
 */
public class SemesterNotExistsException extends RuntimeException {

    public SemesterNotExistsException(String periodId) {
        super(String.format("SEMESTER WITH ID: %s NOT FOUND", periodId));
    }
}
