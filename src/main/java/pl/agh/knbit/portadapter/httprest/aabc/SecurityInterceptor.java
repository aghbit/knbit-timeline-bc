package pl.agh.knbit.portadapter.httprest.aabc;

import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import pl.agh.knbit.portadapter.httprest.adapters.springmvc.Principal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

/**
 * @author Lukasz Raduj 2015 raduj.lukasz at gmail.com
 */
@Component
public class SecurityInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    private AuthBcClient authBcClient;

    @Value("${endpoints.aa-bc.auth-header.name}")
    private String authHeaderName;

    @Override
    public boolean preHandle(@NonNull HttpServletRequest request,
                             @NonNull HttpServletResponse response,
                             @NonNull Object handler) throws Exception {

        HandlerMethod handlerMethod;
        if (handler instanceof HandlerMethod) {
            handlerMethod = (HandlerMethod) handler;
        } else {
            return true;
        }

        if (arePermissionsRequired(handlerMethod)) {
            Boolean validToken = Optional.ofNullable(request.getHeader(authHeaderName))
                    .filter(authBcClient::validateToken)
                    .map(authToken -> validatePermission(authToken, handlerMethod))
                    .orElse(Boolean.FALSE);

            if (validToken) {
                request.setAttribute(Principal.ARGUMENT_NAME, Principal.ofValid());
                return true;
            } else {
                request.setAttribute(Principal.ARGUMENT_NAME, Principal.ofInvalid());
                return true;
            }
        }

        if (isAuthenticationRequired(handlerMethod)) {
            Boolean tokenIsValid = Optional.ofNullable(request.getHeader(authHeaderName))
                    .map(authBcClient::validateToken)
                    .orElse(Boolean.FALSE);

            if (tokenIsValid) {
                return true;
            } else {
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                return false;
            }
        }

        return true;
    }

    private boolean isAuthenticationRequired(HandlerMethod handlerMethod) {
        return Optional.ofNullable(handlerMethod)
                .map(handler -> handler.getMethodAnnotation(Authenticated.class))
                .isPresent();
    }

    private boolean arePermissionsRequired(HandlerMethod handlerMethod) {
        return Optional.ofNullable(handlerMethod)
                .map(handler -> handler.getMethodAnnotation(Authorized.class))
                .isPresent();
    }

    private boolean validatePermission(@NonNull String authToken, @NonNull HandlerMethod handler) {
        return Optional.of(handler)
                .map(handlerMethod -> handlerMethod.getMethodAnnotation(Authorized.class))
                .map(Authorized::permission)
                .map((neededPermission) -> authBcClient.validatePermission(authToken, neededPermission))
                .orElse(Boolean.TRUE);
    }
}
