package pl.agh.knbit.portadapter.httprest.adapters.springmvc.tmp;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.agh.knbit.domain.semester.Member;
import pl.agh.knbit.domain.semester.Semester;
import pl.agh.knbit.domain.semester.SemesterId;
import pl.agh.knbit.domain.semester.event.Event;
import pl.agh.knbit.domain.semester.event.EventId;
import pl.agh.knbit.domain.semester.event.EventType;
import pl.agh.knbit.domain.semester.project.Project;
import pl.agh.knbit.domain.semester.project.ProjectId;
import pl.agh.knbit.domain.semester.repository.SemesterRepository;
import pl.agh.knbit.domain.semester.section.Section;
import pl.agh.knbit.domain.semester.section.SectionId;
import pl.agh.knbit.domain.semester.systemevents.SemesterFactory;

import javax.annotation.PostConstruct;
import java.util.Set;
import java.util.UUID;
import java.util.stream.IntStream;

import static com.google.common.collect.Sets.newHashSet;
import static java.util.stream.Collectors.toSet;
import static pl.agh.knbit.domain.semester.project.ProjectState.IN_PROGRESS;

/**
 * Only for GUI development time. Should be removed later.
 *
 * @author Lukasz Raduj 2015 <raduj.lukasz at gmail.com>
 */
@Profile("!test")
@Component
@Slf4j
public class DbDataPopulatorBean {
    private final SemesterRepository semesterRepository;
    private final SemesterFactory semesterFactory;
    public static final SemesterId SEMESTER_ID = SemesterId.of("2014-SUMMER");

    @Autowired
    public DbDataPopulatorBean(SemesterRepository semesterRepository, SemesterFactory semesterFactory) {
        this.semesterRepository = semesterRepository;
        this.semesterFactory = semesterFactory;
    }

    @PostConstruct
    @Transactional
    public void addPeriod() {
        Section ideaFactory = Section.builder()
                .name("Idea Factory")
                .description("Sekcja Idea Factory opiera swoją działalność na trzech wartościach:\n" +
                        "\n" +
                        "Kreatywnym rozwiązywaniu realnych, dotykających nas problemów\n" +
                        "Zdobywaniu praktycznego doświadczenia w pracy zespołowej\n" +
                        "Wspólnej nauce od siebie nawzajem")
                .sectionId(SectionId.of("e7d5846d-a5e3-467a-9339-cec8616161bf"))
                .memberCount(45)
                .logoUrl("http://knbit.edu.pl/media/storage/images/members/sections/iflogo_3.png")
                .build();

        Section ai = Section.builder()
                .name("AI")
                .description("Od początku rozwoju komputerów marzymy o inteligentnych maszynach, które będą nam pomagać w codziennym życiu. " +
                        "Budziło to jednocześnie fascynację, wątpliwości i przerażenie. " +
                        "Czy maszyny faktycznie potrafią myśleć? Jeżeli tak, to jak możemy tego dokonać? " +
                        "Co, jeżeli stworzymy inteligentne komputery, które następnie zbuntują się i będą chciały przejąć nad nami kontrolę? " +
                        "Pytania te towarzyszą nam już od dziesięcioleci, odzwierciedlając się także w kulturze: filmach i książkach.")
                .sectionId(SectionId.of("1e948a54-3b26-48d0-b9f3-f4b7eced0f42"))
                .memberCount(34)
                .logoUrl("http://tinyurl.com/q3olpc2")
                .build();


        Section bitAlgo = Section.builder()
                .name("Bit Algo")
                .description("Sekcja zrzeszająca fanów algorytmiki.")
                .sectionId(SectionId.of("bc38af4d-5206-4db4-84ab-ed144e77d8bc"))
                .memberCount(100)
                .logoUrl("http://knbit.edu.pl/media/storage/images/members/sections/algo.png")
                .build();


        Section dotNot = Section.builder()
                .name(".net")
                .description("Nasza grupa skupia się wokół ludzi zainteresowanych technologiami oferowanymi przez firmę Microsoft, " +
                        "ze szczególnym uwzględnieniem platformy .NET. " +
                        "Uczymy się jak wykorzystać najnowsze technologie dostępne na rynku w tym Universal Apps, ASP.NET, " +
                        "Windows Azure Cloud Computing, Internet Of Things - Windows on Devices. " +
                        "Wiedza zdobyta podczas zajęć wykładowych jest doskonalona podczas zajęć praktycznych. " +
                        "Kolejnymi krokami podejmowanymi przez członków naszej sekcji są konkursy, szkolenia oraz projekty " +
                        "w jakich aktywnie bierzemy udział.")
                .sectionId(SectionId.of("35aaabc8-2538-4951-a781-698b32d2cd3b"))
                .memberCount(5)
                .logoUrl("http://wsscg.blob.core.windows.net/resources/grupy/93/images/big_logo.jpg")
                .build();

        Section sfi = Section.builder()
                .name("SFI")
                .description("Studencki Festiwal Informatyczny to doroczna konferencja informatyczna organizowana przez " +
                        "samych studentów i do nich głównie kierowana. Odbywa się regularnie od roku 2005 i jest efektem " +
                        "porozumienia pomiędzy kołami naukowymi BIT AGH oraz kołami naukowymi z Uniwersytetu Jagiellońskiego, " +
                        "Politechniki Krakowskiej i Uniwersytetu Ekonomicznego w Krakowie. W edycji 7., 9. oraz 10. " +
                        "Koło BIT i Akademia Górniczo-Hutnicza byligospodarzami festiwalu w Centrum Dydaktyki AGH.")
                .sectionId(SectionId.of("ec22ffff-bcb8-42cb-be8b-03cd1b042db2"))
                .memberCount(90)
                .logoUrl("http://sfi.org.pl/wp-content/themes/SFI/img/logo.png")
                .build();

        Project getInTime = Project.builder()
                .projectId(ProjectId.of(randomId()))
                .projectState(IN_PROGRESS)
                .name("Get in Time!")
                .description("Time management system")
                .sectionId(ideaFactory.sectionId())
                .participants(generateRandomMemberIds(30))
                .logoUrl("http://knbit.edu.pl/media/storage/projects/logos/getintime-0cdbc65b8c5a7fff9e4a32d79387970a.png")
                .build();

        Project meetly = Project.builder()
                .projectId(ProjectId.of(randomId()))
                .projectState(IN_PROGRESS)
                .name("Meetly")
                .description("Wieloplatformowe narzędzie planowania spotkań. Pozwala zaoszczędzić czas pracowników " +
                        "poprzez uniknięcie konieczności wypełniania potężnych \"doodli\". Wykorzystując w sposób " +
                        "inteligentny i bezpieczny prywatne kalendarze daje managerom możliwość wybrania odpowiedniego " +
                        "terminu na spotkanie.")
                .sectionId(ideaFactory.sectionId())
                .participants(generateRandomMemberIds(50))
                .logoUrl("http://knbit.edu.pl/media/storage/projects/logos/meetly2_4.png")
                .build();

        Project codeMorph = Project.builder()
                .projectId(ProjectId.of(randomId()))
                .projectState(IN_PROGRESS)
                .name("CodeMorph")
                .description("CodeMorph is a software development tool consisting of the Analyzer that accepts two " +
                        "versions of the same code as an input and produces a hierarchical set of transitions between " +
                        "those two snapshots of code. Those transitions are then passed as an input to Players that " +
                        "can replay those changes in a human-readable and more importantly human-like way.")
                .sectionId(ai.sectionId())
                .participants(generateRandomMemberIds(50))
                .logoUrl("http://knbit.edu.pl/media/storage/projects/logos/codemorph-83afe9d0ec84b9646eadd5ac0fa2dde5.png")
                .build();

        Set<Member> speakers = generateRandomMemberIds(2);
        Event kafkaIntro = Event.builder()
                .description("Will be awesome! Come all")
                .sectionId(ideaFactory.sectionId())
                .eventType(EventType.LECTURE)
                .eventId(EventId.of(randomId()))
                .title("Apache Kafka introduction")
                .speakers(speakers)
                .pictureUrl("http://hortonworks.com/wp-content/uploads/2014/08/kafka-logo-wide.png")
                .build();

        Event appsDeveloper = Event.builder()
                .title("Od zera do universal Apps Developera - IX")
                .description("Witam, zapraszam na kolejne spotkanie z cyklu Universal Apps. " +
                        "Wreszcie dotarliśmy do platformy Windows 8.1 / Windows Phone. " +
                        "W trakcie spotkania dowiecie się jakie są założenia aplikacji na Windows'a oraz dlaczego " +
                        "możemy tworzyć aplikacje uniwersalne. Po tym wstępie teoretycznym omówimy zagadnienia " +
                        "związane z wytwarzaniem aplikacji na tą platformę. W szczególności: 1. " +
                        "Architektura aplikacji 2. Cykl życia aplikacji 3. Nawigowanie po aplikacji 4. " +
                        "Budowanie aplikacji 4.1. Interfejs użytkownika 4.2. Code-Behind 4.3. MVVM 5. Nasz wspólny projekt.\n" +
                        "\n" +
                        "Zapraszam jeszcze raz do wspólnej zabawy przy tworzeniu :)")
                .sectionId(dotNot.sectionId())
                .eventType(EventType.LECTURE)
                .eventId(EventId.of(randomId()))
                .speakers(speakers)
                .pictureUrl("http://stringfeed.xyz/wp-content/uploads/2015/09/top-5-must-have-android-apps-recommendations-busy-phone.jpg")
                .build();

        Event deepLearningMethods = Event.builder()
                .title("Metody Deep Learning")
                .description("Chcesz dowiedzieć się jak działa?:\n" +
                        "\n" +
                        "system rekomendacyjny na Netflix\n" +
                        "rozpoznawanie mowy w Baidu\n" +
                        "wykrywanie oszustw w Paypalu\n" +
                        "wykrywanie twarzy na Facebooku")
                .sectionId(ai.sectionId())
                .eventId(EventId.of(randomId()))
                .eventType(EventType.WORKSHOP)
                .speakers(generateRandomMemberIds(1))
                .pictureUrl("http://www.alchemyapi.com/sites/default/files/deepLearningAI500.png")
                .build();


        Event googleTechTalk = Event.builder()
                .title("Google Big Data tech talk")
                .description("Zapraszamy was na wykład dotyczący ogólnie pojętej tematyki Big Data, który poprowadzi " +
                        "inżynier z krakowskiego biura Google, na co dzień zajmujący się tym tematem w swojej pracy. " +
                        "Na bazie swoich doświadczeń z kluczowych dla firmy projektów wprowadzi was w świat " +
                        "przetwarzania ogromnej ilości danych zaczynając od samych podstaw i potrzeb a kończąc " +
                        "na konkretnych rozwiązaniach takich jak wszechobecny Map Reduce.")
                .sectionId(bitAlgo.sectionId())
                .eventType(EventType.LECTURE)
                .eventId(EventId.of(randomId()))
                .pictureUrl("http://s11.whatnext.pl/wp-content/uploads/2015/09/GOOGLElogo1.png")
                .build();

        Semester semester = new Semester(SEMESTER_ID);

        semester.addEvents(newHashSet(kafkaIntro, googleTechTalk, appsDeveloper, deepLearningMethods));
        semester.addProjects(newHashSet(getInTime, meetly, codeMorph));
        semester.addSections(newHashSet(ideaFactory, ai, dotNot, sfi, bitAlgo));
        semester.addMembers(generateRandomMemberIds(330));
        semester.increaseCommitsCount(2199);
        semester.activate();

        if (!semesterRepository.exists(SEMESTER_ID)) {
            log.info("Populating DB with seed data");
            semesterRepository.save(semester);
        } else {
            log.info("Semester with id {} already exists! Seeding stop.", SEMESTER_ID);
        }

        SemesterId next = SEMESTER_ID.next();
        if (!semesterRepository.exists(next)) {
            Semester draftSemester = semesterFactory.newDraftFromPreviousSemester(next);
            draftSemester.activate();
            semesterRepository.save(draftSemester);
        }
    }

    private Set<Member> generateRandomMemberIds(int howMany) {
        return IntStream.range(0, howMany)
                .mapToObj(i -> Member.of(randomId()))
                .collect(toSet());
    }

    private String randomId() {
        return UUID.randomUUID().toString();
    }
}
