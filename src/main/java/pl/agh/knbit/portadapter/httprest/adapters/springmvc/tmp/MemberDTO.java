package pl.agh.knbit.portadapter.httprest.adapters.springmvc.tmp;

import lombok.Value;

/**
 * @author Lukasz Raduj 2015 raduj.lukasz at gmail.com
 */
@Value
public class MemberDTO {
    String name;
    String lastName;
    String photoUrl;
    String role;
}
