package pl.agh.knbit.portadapter.httprest.bitbucket;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * @author Lukasz Raduj 2015 raduj.lukasz at gmail.com
 */
@Slf4j
@Component
public class BitBucketClient {
    private final static String KN_BIT_USER = "aghbit";
    private final static String USERS_REPOS_URL = "https://bitbucket.org/api/1.0/users/" + KN_BIT_USER;
    private final static String REPO_CHANGE_SET_URL_PREFIX = "https://bitbucket.org/api/1.0/repositories/" + KN_BIT_USER + "/";
    private final static String REPO_CHANGE_SET_URL_POSTFIX = "/changesets/";

    private final RestTemplate restTemplate;

    @Autowired
    public BitBucketClient(@Qualifier("bitBucketRestTemplate") RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    private List<RepositoryDTO> findAllUserRepositories() {
        UserProfileDTO userProfileDTO = restTemplate.getForObject(USERS_REPOS_URL, UserProfileDTO.class);

        return userProfileDTO.getRepositories();
    }

    private String buildGetRepoChangeSetUrl(String repoSlug) {
        return REPO_CHANGE_SET_URL_PREFIX + repoSlug + REPO_CHANGE_SET_URL_POSTFIX;
    }

    private long getCommitsCountInRepo(String repoSlug) {
        try {
            ChangeSetDTO changeSetDTO = restTemplate.getForObject(buildGetRepoChangeSetUrl(repoSlug), ChangeSetDTO.class);
            return changeSetDTO.getCount();
        } catch (Exception ex) {
            //BitBucket API throws 404 when repo exists, but has no commit! Shame on you BitBucket.
            return 0;
        }
    }

    public long countAllCommits() {
        log.info("Counting all commits for {} user in BitBucket...", KN_BIT_USER);
        Long allCommits = findAllUserRepositories()
                .parallelStream()
                .peek(repoDTO -> log.debug("Counting commits in {} repo", repoDTO))
                .map(RepositoryDTO::getSlug)
                .map(this::getCommitsCountInRepo)
                .reduce(0L, (a, b) -> a + b);

        log.info("Total commits count in BitBucket: {}", allCommits);
        return allCommits;
    }
}


