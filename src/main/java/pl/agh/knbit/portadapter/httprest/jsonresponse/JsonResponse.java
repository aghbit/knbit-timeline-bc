package pl.agh.knbit.portadapter.httprest.jsonresponse;

import lombok.Value;

@Value
public class JsonResponse<T> {
    private static final String SUCCESS_MESSAGE = "Success";
    private final boolean wasSuccessful;
    private final T data;
    private final String message;

    public static <T> JsonResponse<Iterable<T>> ofSuccess(Iterable<T> iterableData) {
        return new JsonResponse<>(true, iterableData, SUCCESS_MESSAGE);
    }

    public static <T> JsonResponse<T> ofSuccess(T singleData) {
        return new JsonResponse<>(true, singleData, SUCCESS_MESSAGE);
    }

    public static <T> JsonResponse<T> ofSuccess() {
        final T EMPTY_DATA = null;
        return new JsonResponse<>(true, EMPTY_DATA, SUCCESS_MESSAGE);
    }

    public static <T> JsonResponse<T> ofException(Exception ex) {
        final T EMPTY_DATA = null;
        return new JsonResponse<>(false, EMPTY_DATA, ex.getMessage());
    }
}
