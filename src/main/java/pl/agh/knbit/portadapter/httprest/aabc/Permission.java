package pl.agh.knbit.portadapter.httprest.aabc;

/**
 * @author Lukasz Raduj 2015 raduj.lukasz at gmail.com
 */
public enum Permission {
    READ_DRAFT_SEMESTER("ADMIN"), //todo ask aa-bc to create special permission
    READ_FULL_EVENTS_FEED("ADMIN");

    private String value;

    Permission(String value) {
        this.value = value;
    }

    public String value() {
        return this.value;
    }
}
