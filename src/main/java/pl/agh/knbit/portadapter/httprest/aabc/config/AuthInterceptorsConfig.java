package pl.agh.knbit.portadapter.httprest.aabc.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import pl.agh.knbit.portadapter.httprest.aabc.PrincipalArgumentResolver;
import pl.agh.knbit.portadapter.httprest.aabc.SecurityInterceptor;

import java.util.List;

/**
 * @author Lukasz Raduj 2015 raduj.lukasz at gmail.com
 */
@Configuration
public class AuthInterceptorsConfig extends WebMvcConfigurerAdapter {

    @Autowired
    private SecurityInterceptor securityInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(securityInterceptor);

        super.addInterceptors(registry);
    }

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        argumentResolvers.add(new PrincipalArgumentResolver());
    }
}
