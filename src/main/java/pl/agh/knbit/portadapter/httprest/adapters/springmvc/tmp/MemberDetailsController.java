package pl.agh.knbit.portadapter.httprest.adapters.springmvc.tmp;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.agh.knbit.portadapter.httprest.jsonresponse.JsonResponse;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * @author Lukasz Raduj 2015 raduj.lukasz at gmail.com
 */
@RestController
public class MemberDetailsController {

    private Iterator<MemberDTO> memberIterator;

    public MemberDetailsController() {
        this.memberIterator = new RoundRobin<>(members()).iterator();
    }

    @RequestMapping(value = "/members/{memberId}",
            method = GET,
            produces = APPLICATION_JSON_VALUE
    )
    public JsonResponse<MemberDTO> loadMemberDetails(@PathVariable String memberId) {
        return JsonResponse.ofSuccess(memberIterator.next());
    }

    public List<MemberDTO> members() {
        MemberDTO jacek = new MemberDTO("Jacek", "Żyła",
                "https://lh3.googleusercontent.com/-jkRiSH7shck/AAAAAAAAAAI/AAAAAAAAAAA/edA2RIWbH68/s128-c-k/photo.jpg",
                "Best UI Developer");

        MemberDTO lukasz = new MemberDTO("Łukasz", "Raduj",
                "https://avatars3.githubusercontent.com/u/3943378?v=3&s=460",
                "Developer");

        MemberDTO szynom = new MemberDTO("Szymon", "Seget",
                "https://media.licdn.com/mpr/mpr/shrinknp_400_400/p/4/000/152/199/0a91c29.jpg",
                "Przewodniczący");

        MemberDTO slawek = new MemberDTO("Sławek", "Nowak",
                "https://pbs.twimg.com/profile_images/378800000693633156/fea6c2c62c02c0f924cc15c763c7e55c.jpeg",
                "Section Lead");

        MemberDTO kamil = new MemberDTO("Kamil", "Kulak",
                "https://media.licdn.com/mpr/mpr/shrinknp_400_400/AAEAAQAAAAAAAAKkAAAAJDJkZWEzNWUxLWVjYmItNGFmOS04NjZiLWI2MTdlYmViOTllNA.jpg",
                "Developer");

        MemberDTO adam = new MemberDTO("Adam", "Zima",
                "http://www2.im.uj.edu.pl/kmm/uczestnicy//uczestnicy/t-z/zima-a.jpg",
                "Devloper");

        MemberDTO seba = new MemberDTO("Sebastian", "Zuchmański",
                "https://media.licdn.com/mpr/mpr/shrinknp_400_400/p/8/005/0b4/00c/0fcf572.jpg",
                "Developer");

        return Arrays.asList(jacek, lukasz, szynom, slawek, kamil, adam, seba);
    }


    static class RoundRobin<T> implements Iterable<T> {
        private List<T> coll;

        public RoundRobin(List<T> coll) {
            this.coll = coll;
        }

        public Iterator<T> iterator() {
            return new Iterator<T>() {
                private int index = 0;

                @Override
                public boolean hasNext() {
                    return true;
                }

                @Override
                public T next() {
                    T res = coll.get(index);
                    index = (index + 1) % coll.size();
                    return res;
                }

                @Override
                public void remove() {
                    throw new UnsupportedOperationException();
                }

            };
        }
    }
}