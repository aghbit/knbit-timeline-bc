package pl.agh.knbit.portadapter.httprest.bitbucket;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class BitBucketAuthInterceptor implements ClientHttpRequestInterceptor {

    @Value("${bitbucket.credentialsInBase64}")
    private String credentialsInBase64;

    @Override
    public ClientHttpResponse intercept(HttpRequest request,
                                        byte[] body,
                                        ClientHttpRequestExecution execution) throws IOException {

        HttpHeaders headers = request.getHeaders();
        headers.add("Authorization", "Basic " + credentialsInBase64);

        return execution.execute(request, body);
    }
}