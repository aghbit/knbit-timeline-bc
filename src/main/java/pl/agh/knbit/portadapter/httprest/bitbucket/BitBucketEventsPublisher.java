package pl.agh.knbit.portadapter.httprest.bitbucket;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import pl.agh.knbit.generated.protobuffs.TimelineBc;
import pl.agh.knbit.portadapter.kafka.KafkaEventProducer;

import static pl.agh.knbit.util.DateTimeUtils.nowAsEpochSeconds;

/**
 * @author Lukasz Raduj 2015 raduj.lukasz at gmail.com
 */
@Component
@Slf4j
public class BitBucketEventsPublisher {
    private final BitBucketClient client;
    private final KafkaEventProducer kafkaEventProducer;

    @Autowired
    public BitBucketEventsPublisher(BitBucketClient client,
                                    KafkaEventProducer kafkaEventProducer) {
        this.client = client;
        this.kafkaEventProducer = kafkaEventProducer;
    }

//    @Scheduled(cron = "0 * * * * *") //every minute
    @Scheduled(cron = "0 0 0 * * *") //daily
    public void updateCommitsCount() {
        try {
            long allCommits = client.countAllCommits();

            TimelineBc.TotalCommitsCountUpdateEvent event = TimelineBc.TotalCommitsCountUpdateEvent
                    .newBuilder()
                    .setCommitsCount(allCommits)
                    .setUtcDateAsEpochSeconds(nowAsEpochSeconds())
                    .build();
            kafkaEventProducer.publish(event);

        } catch (Exception ex) {
            log.error("Cannot update commits count!", ex);
        }
    }
}
