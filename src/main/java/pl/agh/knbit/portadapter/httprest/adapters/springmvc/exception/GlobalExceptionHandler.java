package pl.agh.knbit.portadapter.httprest.adapters.springmvc.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import pl.agh.knbit.portadapter.httprest.jsonresponse.JsonResponse;

/**
 * @author Lukasz Raduj <raduj.lukasz@gmail.com>
 */
@ControllerAdvice
public class GlobalExceptionHandler {
    private static final Logger LOG = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public JsonResponse handleEverything(Exception e) {
        LOG.error("Exception occurred after web request", e);

        return JsonResponse.ofException(e);
    }
}
