package pl.agh.knbit.portadapter.httprest.bitbucket.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;
import pl.agh.knbit.portadapter.httprest.bitbucket.BitBucketAuthInterceptor;

import java.util.Collections;

/**
 * @author Lukasz Raduj 2015 raduj.lukasz at gmail.com
 */
@Configuration
@EnableScheduling
public class BitBucketConfig {

    @Bean
    public RestTemplate bitBucketRestTemplate(BitBucketAuthInterceptor interceptor) {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setInterceptors(Collections.singletonList(interceptor));

        return restTemplate;
    }
}
