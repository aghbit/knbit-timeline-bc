package pl.agh.knbit.portadapter.httprest.adapters.springmvc;

import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.experimental.Accessors;

/**
 * @author Lukasz Raduj 2015 raduj.lukasz at gmail.com
 */
@Value
@RequiredArgsConstructor
@Accessors(fluent = true)
public class Principal {
    public final static String ARGUMENT_NAME = "principal";
    private final boolean hasRequiredPermissions;

    public Principal() {
        this.hasRequiredPermissions = false;
    }

    public static Principal ofValid() {
        return new Principal(true);
    }

    public static Principal ofInvalid() {
        return new Principal(false);
    }
}
