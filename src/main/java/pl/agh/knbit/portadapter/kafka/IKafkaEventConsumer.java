package pl.agh.knbit.portadapter.kafka;

import rx.Observable;

public interface IKafkaEventConsumer {

    Observable consume(String topicName, boolean fromBeginning);

    default Observable consume(String topicName) {
        return consume(topicName, false);
    }
}
