package pl.agh.knbit.portadapter.kafka.config;

import kafka.consumer.Consumer;
import kafka.consumer.ConsumerConfig;
import kafka.javaapi.consumer.ConsumerConnector;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;

import java.util.Properties;

/**
 * @author Lukasz Raduj 2015 raduj.lukasz at gmail.com
 */
@Configuration
public class KafkaConfig {

    @Value("${endpoints.kafka.bootstrap.servers}")
    private String bootstrapServers;

    @Value("${endpoints.kafka.zookeeper.connect}")
    private String zookeeperConnect;

    @Bean(destroyMethod = "close")
    public KafkaProducer<String, byte[]> kafkaProducer() {
        Properties props = new Properties();
        props.put("bootstrap.servers", bootstrapServers);
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.ByteArraySerializer");

        return new KafkaProducer<>(props);
    }

    @Bean(destroyMethod = "shutdown")
    public ConsumerConnector consumerConnector() {
        Properties props = new Properties();
        props.put("zookeeper.connect", zookeeperConnect);
        props.put("group.id", "test-group");
        props.put("zookeeper.session.timeout.ms", "413");
        props.put("zookeeper.sync.time.ms", "203");
        props.put("auto.commit.interval.ms", "1000");
        props.put("advertised.host.name", "192.168.99.100");
        props.put("advertised.port", "2181");


        ConsumerConfig cf = new ConsumerConfig(props);
        return Consumer.createJavaConsumerConnector(cf);
    }

    @Bean(destroyMethod = "shutdown")
    @Primary
    public ConsumerConnector reportsConsumerConnector() {
        Properties props = new Properties();
        props.put("zookeeper.connect", zookeeperConnect);
        props.put("group.id", "rep");
        props.put("zookeeper.session.timeout.ms", "413");
        props.put("zookeeper.sync.time.ms", "203");
        props.put("auto.commit.interval.ms", "1000");
//        props.put("autooffset.reset", "smallest");

        ConsumerConfig cf = new ConsumerConfig(props);
        return Consumer.createJavaConsumerConnector(cf);
    }


    @Bean
    @Scope("prototype")
    public Properties kafkaConsumerProperties() {
        Properties props = new Properties();
        props.put("zookeeper.connect", zookeeperConnect);
//        props.put("group.id", "rep");
        props.put("zookeeper.session.timeout.ms", "413");
        props.put("zookeeper.sync.time.ms", "203");
        props.put("auto.commit.interval.ms", "1000");

        return props;
    }
}
