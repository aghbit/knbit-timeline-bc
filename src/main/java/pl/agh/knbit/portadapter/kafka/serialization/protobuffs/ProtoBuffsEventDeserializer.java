package pl.agh.knbit.portadapter.kafka.serialization.protobuffs;

import com.google.protobuf.MessageLite;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Method;

/**
 * @author Lukasz Raduj 2015 raduj.lukasz at gmail.com
 */
@Slf4j
public class ProtoBuffsEventDeserializer {

    public static <T extends MessageLite> T deserialize(byte[] bytes, Class<T> type) {
        try {
            Method parseFrom = type.getMethod("parseFrom", byte[].class);

            return (T) parseFrom.invoke(type, bytes);
        } catch (Exception ex) {
            log.error("Cannot deserialize proto buffs message! Reason: {}", ex);
            return null;
        }
    }
}
