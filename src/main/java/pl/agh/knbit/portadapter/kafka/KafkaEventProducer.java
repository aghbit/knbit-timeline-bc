package pl.agh.knbit.portadapter.kafka;

import com.google.protobuf.MessageLite;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.agh.knbit.portadapter.kafka.serialization.protobuffs.ProtoBuffsEventSerializer;

import java.util.concurrent.Future;

@Slf4j
@Component
public class KafkaEventProducer {
    private final KafkaProducer<String, byte[]> producer;

    @Autowired
    public KafkaEventProducer(KafkaProducer<String, byte[]> producer) {
        this.producer = producer;
    }

    public <T extends MessageLite> Future publish(T event) {
        String topic = event.getClass().getSimpleName();
        ProducerRecord<String, byte[]> data =
                new ProducerRecord<>(topic, ProtoBuffsEventSerializer.serialize(event));

        log.info("Sending event to topic {}", topic);
        return producer.send(data);
    }
}
