package pl.agh.knbit.portadapter.kafka.serialization.protobuffs;

import com.google.protobuf.MessageLite;

/**
 * @author Lukasz Raduj 2015 raduj.lukasz at gmail.com
 */
public class ProtoBuffsEventSerializer {

    public static <T extends MessageLite> byte[] serialize(T event) {
        return event.toByteArray();
    }
}
