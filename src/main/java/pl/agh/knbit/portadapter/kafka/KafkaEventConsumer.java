package pl.agh.knbit.portadapter.kafka;

import com.google.common.collect.ImmutableMap;
import com.google.protobuf.MessageLite;
import kafka.consumer.ConsumerIterator;
import kafka.consumer.KafkaStream;
import kafka.javaapi.consumer.ConsumerConnector;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import pl.agh.knbit.portadapter.kafka.serialization.protobuffs.ProtoBuffsEventDeserializer;
import rx.Observable;
import rx.Subscriber;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Slf4j
@Component
public class KafkaEventConsumer {
    private final ConsumerConnector consumer;
    private final ExecutorService executor;
    private Map<String, List<KafkaStream<byte[], byte[]>>> messageStreams;
    private final Object guard = new Object();

    @Autowired
    public KafkaEventConsumer(@Qualifier("consumerConnector") ConsumerConnector consumer) {
        this.consumer = consumer;
        this.executor = Executors.newFixedThreadPool(10);
    }

    public <T extends MessageLite> Observable<T> observe(Class<T> eventSubscriber) {
        return Observable.create(subscriber -> {
            Runnable r = createRunnableListener(eventSubscriber, subscriber);
            executor.execute(r);
        });
    }

    public void initTopicsMap(Set<Class<? extends MessageLite>> eventsTypes) {
        ImmutableMap.Builder<String, Integer> topicCountMapBuilder = ImmutableMap.builder();

        eventsTypes.stream()
                .forEach(sub -> topicCountMapBuilder.put(sub.getSimpleName(), 1));

        this.messageStreams = consumer.createMessageStreams(topicCountMapBuilder.build());
        log.info("Message streams created! {}", messageStreams);
    }


    private <T extends MessageLite> Runnable createRunnableListener(Class<T> eventType,
                                                                    Subscriber<? super T> subscriber) {
        return () -> {
            try {
                String topicName = eventType.getSimpleName();
                List<KafkaStream<byte[], byte[]>> streams = messageStreams.get(topicName);

                ConsumerIterator<byte[], byte[]> it = streams.get(0)
                        .iterator();

                log.info("Listening on {} events", topicName);
                while (it.hasNext()) {
                    synchronized (guard) {
                        subscriber.onNext(
                                ProtoBuffsEventDeserializer.deserialize(it.next().message(), eventType)
                        );
                    }
                }
            } catch (Exception ex) {
                subscriber.onError(ex);
            }
        };
    }
}
