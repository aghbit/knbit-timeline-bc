package pl.agh.knbit.portadapter.kafka.controller;


import com.google.common.collect.Sets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.agh.knbit.generated.protobuffs.*;
import pl.agh.knbit.portadapter.kafka.KafkaEventProducer;
import pl.agh.knbit.util.DateTimeUtils;

import java.util.UUID;

import static java.util.Objects.isNull;

/**
 * Only for tests purposes.
 *
 * @author Lukasz Raduj 2015 raduj.lukasz at gmail.com
 */
@RestController
public class KafkaController {

    private final KafkaEventProducer eventProducer;

    @Autowired
    public KafkaController(KafkaEventProducer eventProducer) {
        this.eventProducer = eventProducer;
    }

    @RequestMapping(value = "/kafka/members/{memberId}", method = RequestMethod.GET)
    public String pushMessage(@PathVariable String memberId) {
        MembersBc.MemberJoinedKNBITEvent event = MembersBc.MemberJoinedKNBITEvent.newBuilder()
                .setMemberId(memberId)
                .setUtcDateAsEpochSeconds(DateTimeUtils.nowAsEpochSeconds())
                .setMemberFirstName(memberId)
                .setMemberLastName(memberId)
                .setMemberEmail(memberId)
                .build();

        eventProducer.publish(event);

        return "Success";
    }

    @RequestMapping(value = "/kafka/members/left/{memberId}", method = RequestMethod.GET)
    public String pushMemberLeftMessage(@PathVariable String memberId) {
        MembersBc.MemberLeftKNBITEvent event = MembersBc.MemberLeftKNBITEvent.newBuilder()
                .setMemberId(memberId)
                .setUtcDateAsEpochSeconds(DateTimeUtils.nowAsEpochSeconds())
                .build();

        eventProducer.publish(event);

        return "Success";
    }

    @RequestMapping(value = "/kafka/commits/{commitCount}", method = RequestMethod.GET)
    public String pushMessageCommitUpdate(@PathVariable int commitCount) {
        TimelineBc.TotalCommitsCountUpdateEvent event = TimelineBc.TotalCommitsCountUpdateEvent.newBuilder()
                .setUtcDateAsEpochSeconds(DateTimeUtils.nowAsEpochSeconds())
                .setCommitsCount(commitCount)
                .build();

        eventProducer.publish(event);

        return "Success";
    }

    @RequestMapping(value = "/kafka/projects/{project-name}", method = RequestMethod.GET)
    public String pushMessageProjectCreated(@PathVariable("project-name") String projectName,
                                            @RequestParam(value = "section-id", required = false) String sectionId) {

        if (isNull(sectionId)) {
            sectionId = "if-id";
        }

        ProjectsBc.ProjectCreatedEvent event = ProjectsBc.ProjectCreatedEvent.newBuilder()
                .setUtcDateAsEpochSeconds(DateTimeUtils.nowAsEpochSeconds())
                .setSectionId(sectionId)
                .setProjectName(projectName)
                .setProjectDescription("This is a project description")
                .setProjectId(projectName)
                .build();


        eventProducer.publish(event);

        return "Success";
    }

    @RequestMapping(value = "/kafka/events", method = RequestMethod.GET)
    public String pushMessageEventsTookPlace(@RequestParam(value = "section-id", required = false) String sectionId,
                                             @RequestParam(value = "is-lecture", required = false) boolean isLecture) {
        EventsBc.EventTookPlaceEvent.Speaker speaker = EventsBc.EventTookPlaceEvent.Speaker
                .newBuilder()
                .setName("Bartek Szczepanik")
                .setAccountId("bartek-id")
                .build();

        if (isNull(sectionId)) {
            sectionId = "if-id";
        }

        EventsBc.EventTookPlaceEvent.EventType eventType = EventsBc.EventTookPlaceEvent.EventType.WORKSHOP;
        if (isLecture) {
            eventType = EventsBc.EventTookPlaceEvent.EventType.LECTURE;
        }

        EventsBc.EventTookPlaceEvent event = EventsBc.EventTookPlaceEvent
                .newBuilder()
                .setUtcDateAsEpochSeconds(DateTimeUtils.nowAsEpochSeconds())
                .setEventName("Twitter Zipkin Intro!")
                .setEventDescription("Awesome event")
                .setAttendesCount(50)
                .setEventType(eventType)
                .setSectionId(sectionId)
                .addAllSpeakers(Sets.newHashSet(speaker))
                .setEventPictureUrl("http://4.bp.blogspot.com/-DsmFcaeMi4U/T9DX_XLMrdI/AAAAAAAAABg/jKF_pXToIEo/s1600/zipkin.jpg")
                .setEventId(UUID.randomUUID().toString())
                .build();

        eventProducer.publish(event);

        return "Success";
    }

    @RequestMapping(value = "/kafka/sections/{section-name}", method = RequestMethod.GET)
    public String pushMessageSectionCreated(@PathVariable("section-name") String sectionName) {

        SectionsBc.SectionCreatedEvent sectionCreatedEvent = SectionsBc.SectionCreatedEvent
                .newBuilder()
                .setUtcDateAsEpochSeconds(DateTimeUtils.nowAsEpochSeconds())
                .setSectionName(sectionName)
                .setSectionDescription("Sekcja AI skupia się na rozwijaniu swoich zainteresowań w obrębie Sztucznej Inteligencji.\n" +
                        "Organizuje kursy wprowadzające do tematyki Sztucznej Inteligencji, tworzy praktyczne\n" +
                        "projekty, rozwiązujące trudne problemy i dające niesamowite rezultaty, organizuje\n" +
                        "tematyczne wykłady i warsztaty, gdzie pokazujemy nowe ciekawe techniki i pomysły\n" +
                        "zastosowań metod sztucznej inteligencji. ")
                .setSectionId(sectionName + "-id")
                .build();

        eventProducer.publish(sectionCreatedEvent);

        return "Success";
    }

    @RequestMapping(value = "/kafka/sections/{section-name}/member", method = RequestMethod.GET)
    public String pushMessageMemberJoined(@PathVariable("section-name") String sectionName) {

        SectionsBc.MemberJoinedSectionEvent event = SectionsBc.MemberJoinedSectionEvent.newBuilder()
                .setUtcDateAsEpochSeconds(DateTimeUtils.nowAsEpochSeconds())
                .setSectionId(sectionName + "-id")
                .setMemeberId(sectionName)
                .build();


        eventProducer.publish(event);

        return "Success";
    }
}
