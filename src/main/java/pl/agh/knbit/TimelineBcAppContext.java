package pl.agh.knbit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Lukasz Raduj 2015 <raduj.lukasz at gmail.com>
 */
@SpringBootApplication
public class TimelineBcAppContext {

    public static void main(String[] args) {
        SpringApplication.run(TimelineBcAppContext.class, args);
    }
}
