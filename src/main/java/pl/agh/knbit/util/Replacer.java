package pl.agh.knbit.util;

import org.apache.commons.lang.StringUtils;

import java.util.function.Supplier;

public class Replacer {
    private String pattern;

    private Replacer(String pattern) {
        this.pattern = pattern;
    }

    public static Replacer replace(String pattern) {
        return new Replacer(pattern);
    }

    public Replacer with(String from, String to) {
        this.pattern = pattern.replace(from, to);
        return this;
    }

    public Replacer with(String from, String to, String defaultValue) {
        return with(from, to, defaultValue, () -> !StringUtils.isEmpty(to));
    }

    public Replacer with(String from, String to, String defaultValue, Supplier<Boolean> hasValue) {
        if(hasValue.get()) {
            return with(from, to);
        } else {
            return with(from, defaultValue);
        }
    }

    public String toString() {
        return pattern;
    }
}
