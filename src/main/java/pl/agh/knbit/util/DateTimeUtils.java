package pl.agh.knbit.util;

import com.google.protobuf.MessageLite;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

/**
 * @author Lukasz Raduj 2015 raduj.lukasz at gmail.com
 */
public class DateTimeUtils {
    private final static ZoneOffset ZONE_OFFSET = ZoneOffset.UTC;

    public static LocalDateTime fromEpochSeconds(long epochSeconds) {
        return LocalDateTime.ofEpochSecond(epochSeconds, 0, ZONE_OFFSET);
    }

    public static long nowAsEpochSeconds() {
        return LocalDateTime.now().toEpochSecond(ZONE_OFFSET);
    }

    public static long toEpochSeconds(LocalDateTime dateTime) {
        return dateTime.toEpochSecond(ZONE_OFFSET);
    }

    public static <T extends MessageLite> LocalDateTime getEventOccurrenceDate(T event) throws NoSuchMethodException,
            IllegalAccessException,
            InvocationTargetException {
        Method parseFrom = event.getClass().getMethod("getUtcDateAsEpochSeconds");
        return fromEpochSeconds((long) parseFrom.invoke(event));
    }
}
